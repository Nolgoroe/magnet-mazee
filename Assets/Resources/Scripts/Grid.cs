﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public int sizeX, sizeZ;
    public GridCell cellPrefab;
    public float gridSpaceOffset = 1f;
    public Vector3 gridOrigin = Vector3.zero;

    public List<GridCell> CellsInWorld;

    [HideInInspector]
    public Vector2[,] cells;

    public GameManager gameManager;

    public void Init()
    {
        Generate();
        gameManager = GameManager.instance;
    }

    public void Generate()
    {
        cells = new Vector2[sizeX, sizeZ];
        for (int x =0; x< sizeX; x++)
        {
            for (int z=0; z<sizeZ; z++)
            {
                Vector3 pos = new Vector3(x * gridSpaceOffset, 0f, z * gridSpaceOffset) + gridOrigin;
                cells[x, z] = new Vector2(pos.x, pos.z);
                //Debug.Log(cells[x, z]);
                CreateCell(cells[x,z],Quaternion.identity);
            }
        }
    }

    public void CreateCell(Vector2 pos, Quaternion rotation)
    {
        GridCell newCell = Instantiate(cellPrefab) as GridCell;
        newCell.transform.position = new Vector3(pos.x, 0, pos.y);
        newCell.transform.parent = transform;
        CellsInWorld.Add(newCell);
    }
}

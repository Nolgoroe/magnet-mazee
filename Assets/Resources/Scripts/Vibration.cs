﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class Vibration : MonoBehaviour
{
    public HapticTypes haptic = HapticTypes.LightImpact;

    public void AppleVibration()
    {
        MMVibrationManager.Haptic(haptic, false, true, this);
    }

    public void FailToRotate()
    {
        MMVibrationManager.Haptic(HapticTypes.Failure);
    }

    public void SuccessfulMagnetization()
    {
        MMVibrationManager.Haptic(HapticTypes.Success);
    }
}

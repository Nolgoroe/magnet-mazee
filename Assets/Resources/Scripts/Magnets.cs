﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnets : MonoBehaviour
{

    public bool IsFalling = true;
    public GridCell CurrentCell;
    public bool beingMagnetized;
    public int minXMovement = 0;
    public int minZMovement= 0;
    public float fallSpeed;
    public bool IsPartOfPlayer = false;

    private int CurrentSteps = 100;

    private GameManager gameManager;

    private GridCell CellToCheck;
    private Vector3 CellToWorkWith;
    private Vector3 CellToStopOn;

    public void Init()
    {
        gameManager = GameManager.instance;
    }

    public int SetDestinationX(Vector3 direction, Magnets ObjectToSetDestination)
    {
        CellToCheck = new GridCell(); /// ASK ANOTHER PROGRAMMER WHAT'S BETTER TO DO HERE
        CurrentSteps = 100;

        //if (PlayerController.PlayerMagnetsArray.Count == 1)
        //{
        //    CellToWorkWith = ObjectToSetDestination.CurrentCell.transform.position;
        //}

        if (direction == Vector3.right)
        {
            for (int i = 0; i < PlayerController.PlayerMagnetsArray.Count; i++)
            {
                minXMovement = 0;

                CellToWorkWith = PlayerController.PlayerMagnetsArray[i].CurrentCell.transform.position;

                if (!PlayerController.PlayerMagnetsArray[i].IsPartOfPlayer)
                {
                    CellToWorkWith = ObjectToSetDestination.CurrentCell.transform.position;
                }


                for (int k = 1; k < 10; k++)
                {
                    CellToCheck.CellPos = new Vector3(CellToWorkWith.x + k, CellToWorkWith.y, CellToWorkWith.z);

                    FindCellToCheck(CellToCheck);

                    if (CellToCheck.isFullWithMagnet)
                    {
                        minXMovement = k - 2;

                        if (ObjectToSetDestination.CompareTag("Player"))
                        {
                            PlayerController.CollidingWithMagnet = true;
                        }

                        if (Mathf.Abs(minXMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minXMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x + k - 2, CellToWorkWith.y, CellToWorkWith.z);
                        }

                        break;
                    }

                    if (CellToCheck.isFullWithWall)
                    {
                        minXMovement = k - 1;

                        if (ObjectToSetDestination.CompareTag("Player"))
                        {
                            PlayerController.CollidingWithWall = true;
                        }

                        if (Mathf.Abs(minXMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minXMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x + k - 1, CellToWorkWith.y, CellToWorkWith.z);
                        }

                        break;
                    }

                    if (CellToCheck.IsFullWithPlayer && !PlayerController.PlayerMagnetsArray[i].IsPartOfPlayer)
                    {
                        minXMovement = k - 1;

                        if (Mathf.Abs(minXMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minXMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x + k - 1, CellToWorkWith.y, CellToWorkWith.z);
                        }

                        break;
                    }
                }
            }
        }

        if (direction == Vector3.left)
        {
            for (int i = 0; i < PlayerController.PlayerMagnetsArray.Count; i++)
            {
                minXMovement = 0;

                CellToWorkWith = PlayerController.PlayerMagnetsArray[i].CurrentCell.transform.position;

                if (!PlayerController.PlayerMagnetsArray[i].IsPartOfPlayer)
                {
                    CellToWorkWith = ObjectToSetDestination.CurrentCell.transform.position;
                }

                for (int k = 1; k < 10; k++)
                {
                    CellToCheck.CellPos = new Vector3(CellToWorkWith.x - k, CellToWorkWith.y, CellToWorkWith.z);

                    FindCellToCheck(CellToCheck);

                    if (CellToCheck.isFullWithMagnet)
                    {
                        minXMovement = -k + 2;

                        if (ObjectToSetDestination.CompareTag("Player"))
                        {
                            PlayerController.CollidingWithMagnet = true;
                        }

                        if (Mathf.Abs(minXMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minXMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x - k + 2, CellToWorkWith.y, CellToWorkWith.z);
                        }
                        break;
                    }

                    if (CellToCheck.isFullWithWall)
                    {
                        minXMovement = -k + 1;


                        if (ObjectToSetDestination.CompareTag("Player"))
                        {
                            PlayerController.CollidingWithWall = true;
                        }

                        if (Mathf.Abs(minXMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minXMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x - k + 1, CellToWorkWith.y, CellToWorkWith.z);
                        }
                        break;
                    }

                    if (CellToCheck.IsFullWithPlayer && !PlayerController.PlayerMagnetsArray[i].IsPartOfPlayer)
                    {
                        minXMovement = -k + 1;

                        if (Mathf.Abs(minXMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minXMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x - k + 1, CellToWorkWith.y, CellToWorkWith.z);
                        }

                        break;
                    }

                }
            }
        }

        if (!ObjectToSetDestination.CompareTag("Magnet"))
        {
            if (ObjectToSetDestination.CompareTag("Player") && CellToStopOn != CellToWorkWith)
            {
                ObjectToSetDestination.CurrentCell.IsFullWithPlayer = false;
            }
        }

        //return CellToStopOn;
        return CurrentSteps;
    }

    public int SetDestinationZ(Vector3 direction, Magnets ObjectToSetDestination)
    {
        CellToCheck = new GridCell(); /// ASK ANOTHER PROGRAMMER WHAT'S BETTER TO DO HERE
        CurrentSteps = 100;

        //if (PlayerController.PlayerMagnetsArray.Count == 1)
        //{
        //    CellToWorkWith = ObjectToSetDestination.CurrentCell.transform.position;
        //}


        if (direction == Vector3.forward)
        {
            for (int i = 0; i < PlayerController.PlayerMagnetsArray.Count; i++)
            {
                minZMovement = 0;

                CellToWorkWith = PlayerController.PlayerMagnetsArray[i].CurrentCell.transform.position;

                if (!PlayerController.PlayerMagnetsArray[i].IsPartOfPlayer)
                {
                    CellToWorkWith = ObjectToSetDestination.CurrentCell.transform.position;
                }

                for (int k = 1; k < 10; k++)
                {
                    CellToCheck.CellPos = new Vector3(CellToWorkWith.x, CellToWorkWith.y, CellToWorkWith.z + k);

                    FindCellToCheck(CellToCheck);

                    if (CellToCheck.isFullWithMagnet)
                    {
                        minZMovement = k - 2;

                        if (ObjectToSetDestination.CompareTag("Player"))
                        {
                            PlayerController.CollidingWithMagnet = true;
                        }

                        if (Mathf.Abs(minZMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minZMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x, CellToWorkWith.y, CellToWorkWith.z + k - 2);
                        }
                        break;
                    }

                    if (CellToCheck.isFullWithWall)
                    {
                        minZMovement = k - 1;


                        if (ObjectToSetDestination.CompareTag("Player"))
                        {
                            PlayerController.CollidingWithWall = true;
                        }

                        if (Mathf.Abs(minZMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minZMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x, CellToWorkWith.y, CellToWorkWith.z + k - 1);
                        }
                        break;
                    }

                    if (CellToCheck.IsFullWithPlayer && !PlayerController.PlayerMagnetsArray[i].IsPartOfPlayer)
                    {
                        minZMovement = k - 1;

                        if (Mathf.Abs(minZMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minZMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x, CellToWorkWith.y, CellToWorkWith.z + k - 1);
                        }
                        break;
                    }
                }

            }
        }

        if (direction == Vector3.back)
        {
            for (int i = 0; i < PlayerController.PlayerMagnetsArray.Count; i++)
            {
                minZMovement = 0;

                CellToWorkWith = PlayerController.PlayerMagnetsArray[i].CurrentCell.transform.position;

                if (!PlayerController.PlayerMagnetsArray[i].IsPartOfPlayer)
                {
                    CellToWorkWith = ObjectToSetDestination.CurrentCell.transform.position;
                }

                for (int k = 1; k < 10; k++)
                {
                    CellToCheck.CellPos = new Vector3(CellToWorkWith.x, CellToWorkWith.y, CellToWorkWith.z - k);

                    FindCellToCheck(CellToCheck);

                    if (CellToCheck.isFullWithMagnet)
                    {
                        minZMovement = -k + 2;

                        if (ObjectToSetDestination.CompareTag("Player"))
                        {
                            PlayerController.CollidingWithMagnet = true;
                        }

                        if (Mathf.Abs(minZMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minZMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x, CellToWorkWith.y, CellToWorkWith.z - k + 2);
                        }
                        break;
                    }

                    if (CellToCheck.isFullWithWall)
                    {
                        minZMovement = -k + 1;


                        if (ObjectToSetDestination.CompareTag("Player"))
                        {
                            PlayerController.CollidingWithWall = true;
                        }

                        if (Mathf.Abs(minZMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minZMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x, CellToWorkWith.y, CellToWorkWith.z -k + 1);
                        }
                        break;
                    }

                    if(CellToCheck.IsFullWithPlayer && !PlayerController.PlayerMagnetsArray[i].IsPartOfPlayer)
                    {
                        minZMovement = -k + 1;

                        if (Mathf.Abs(minZMovement) <= Mathf.Abs(CurrentSteps))
                        {
                            CurrentSteps = minZMovement;

                            CellToStopOn = new Vector3(CellToWorkWith.x, CellToWorkWith.y, CellToWorkWith.z - k + 1);
                        }
                        break;
                    }
                }

            }
        }

        if (!ObjectToSetDestination.CompareTag("Magnet"))
        {
            if (ObjectToSetDestination.CompareTag("Player") && CellToStopOn != CellToWorkWith)
            {
                ObjectToSetDestination.CurrentCell.IsFullWithPlayer = false;
            }
        }

        //return CellToStopOn;
        return CurrentSteps;
    }

    public IEnumerator WaitForFallAnimation()
    {
        yield return null;
    }
    
    public void GetCurrentCell(Vector3 OriginObjectToCheckPos, GameObject ObjectInside, bool StartOfGame)
    {
        for (int i = 0; i < GameManager.instance.GridManager.CellsInWorld.Count; i++)
        {
            if(OriginObjectToCheckPos == new Vector3(GameManager.instance.GridManager.CellsInWorld[i].transform.position.x, OriginObjectToCheckPos.y, GameManager.instance.GridManager.CellsInWorld[i].transform.position.z))
            {
                //Debug.Log("Fuck yea" + OriginObjectToCheckPos + "AND" + GameManager.instance.GridManager.CellsInWorld[i].transform.position);

                if(StartOfGame && ObjectInside.CompareTag("Player"))
                {
                    StartOfGame = false;
                    ObjectInside.GetComponent<PlayerController>().CurrentCell = GameManager.instance.GridManager.CellsInWorld[i];
                    return;
                }

                if (!StartOfGame)
                {
                    if (ObjectInside.CompareTag("Magnet"))
                    {
                        if (!ObjectInside.GetComponent<FieldMagnet>().IsPartOfPlayer)
                        {
                            ObjectInside.GetComponent<FieldMagnet>().CurrentCell = GameManager.instance.GridManager.CellsInWorld[i];
                            return;
                        }
                    }



                    foreach (Magnets M in PlayerController.PlayerMagnetsArray)
                    {
                        if (M.name == ObjectInside.name)
                        {
                            M.CurrentCell = GameManager.instance.GridManager.CellsInWorld[i];
                        }
                    }
                }

                //if (ObjectInside.CompareTag("Player"))
                //{
                //    CurrentCell.CheckFull();
                //}

                return;
            }
        }
    }

    public void FindCellToCheck(GridCell CellToCehck)
    {
        for (int i = 0; i < GameManager.instance.GridManager.CellsInWorld.Count; i++)
        {
            if (CellToCehck.CellPos == new Vector3(GameManager.instance.GridManager.CellsInWorld[i].transform.position.x, GameManager.instance.GridManager.CellsInWorld[i].transform.position.y, GameManager.instance.GridManager.CellsInWorld[i].transform.position.z))
            {
                CellToCehck.isFullWithMagnet = GameManager.instance.GridManager.CellsInWorld[i].isFullWithMagnet;
                CellToCehck.isFullWithWall = GameManager.instance.GridManager.CellsInWorld[i].isFullWithWall;
                CellToCehck.IsFullWithPlayer = GameManager.instance.GridManager.CellsInWorld[i].IsFullWithPlayer;
                return;
            }
        }
    }

    public int FindSpecificCell(Magnets OriginMagnet, Magnets HitMagnet, Vector3 Direction)
    {

        Vector3 PosToCheck = Vector3.zero;

        int AmountToReturn = 0;

        CellToWorkWith = OriginMagnet.CurrentCell.CellPos;

        if (Direction == Vector3.right)
        {
            PosToCheck = new Vector3(CellToWorkWith.x + 1, CellToWorkWith.y, CellToWorkWith.z);

            if (PosToCheck == HitMagnet.CurrentCell.CellPos)
            {
                AmountToReturn = 0;
            }
            else
            {
                AmountToReturn = 1;
            }

        }

        if (Direction == Vector3.left)
        {
            PosToCheck = new Vector3(CellToWorkWith.x - 1, CellToWorkWith.y, CellToWorkWith.z);

            if (PosToCheck == HitMagnet.CurrentCell.CellPos)
            {
                AmountToReturn = 0;
            }
            else
            {
                AmountToReturn = -1;
            }

        }

        if (Direction == Vector3.forward)
        {
            PosToCheck = new Vector3(CellToWorkWith.x, CellToWorkWith.y, CellToWorkWith.z + 1);

            if (PosToCheck == HitMagnet.CurrentCell.CellPos)
            {
                AmountToReturn = 0;
            }
            else
            {
                AmountToReturn = 1;
            }

        }

        if (Direction == Vector3.back)
        {
            PosToCheck = new Vector3(CellToWorkWith.x, CellToWorkWith.y, CellToWorkWith.z - 1);

            if (PosToCheck == HitMagnet.CurrentCell.CellPos)
            {
                AmountToReturn = 0;
            }
            else
            {
                AmountToReturn = -1;
            }

        }

        return AmountToReturn;
    }

    //public IEnumerator TurnOnEndLevelLight(Light LightToFade, float Duration)
    //{
    //    Debug.Log("Lights");

    //    float counter = 0f;

    //    while (counter < Duration)
    //    {
    //        counter += Time.deltaTime;

    //        LightToFade.intensity = Mathf.Lerp(0, 1, 0.01f);
    //    }

    //    yield return null;
    //}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowPlayer : MonoBehaviour
{
    public List<ShadowFieldMagnet> ShadowMagnetsArray;
    public PlayerController player;

    bool CantRotateHitWall;

    Vector3 DirectionOfRay;

    public LayerMask LayersToHit;
    private void Start()
    {
        player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();

    }

    public void RotateShadowPlayerControl()
    {
        RaycastHit Hit;


        RotateShadowPlayer();
        DirectionOfRay = Vector3.zero;
        foreach (ShadowFieldMagnet sm in ShadowMagnetsArray)
        {
            if (!sm.OverlapCheck())
            {
                StartCoroutine(player.CantRotateAnimation());
                DirectionOfRay = sm.WallHitDuringRotation.transform.position - sm.transform.position;
                transform.rotation = Quaternion.Euler(0, player.currentRotation, 0);

                DirectionOfRay.y = 0;

                if (Physics.Raycast(sm.transform.position, DirectionOfRay, out Hit, LayersToHit))
                {
                    //Debug.Log("Object In Hit: " + Hit.transform.name);

                    if (Hit.transform.CompareTag("Wall") || Hit.transform.CompareTag("SpecialTempWall"))
                    {
                        CantRotateHitWall = true;

                        Destroy(Instantiate(player.gamemanager.ui.CantRotateAnim, new Vector3(Hit.point.x, Hit.point.y + 0.7f, Hit.point.z), Quaternion.Euler(new Vector3(90, 0, 0))), 1);
                        PlayerController.isMoving = false;
                        PlayerController.stillRotating = false;
                    }
                }

                if (!CantRotateHitWall)
                {
                    DirectionOfRay = sm.WallHitDuringRotation.transform.position - transform.position;

                    DirectionOfRay.y = 0;

                    if (Physics.Raycast(transform.position, DirectionOfRay, out Hit, LayersToHit))
                    {
                        if (Hit.transform.CompareTag("Wall") || Hit.transform.CompareTag("SpecialTempWall"))
                        {
                            CantRotateHitWall = true;
                            Destroy(Instantiate(player.gamemanager.ui.CantRotateAnim, new Vector3(Hit.point.x, Hit.point.y + 0.7f, Hit.point.z), Quaternion.Euler(new Vector3(90, 0, 0))), 1);
                            PlayerController.isMoving = false;
                            PlayerController.stillRotating = false;
                        }
                    }
                }

                CantRotateHitWall = false;
                return;
            }
        }

        RotateShadowPlayer();
        foreach (ShadowFieldMagnet sm in ShadowMagnetsArray)
        {
            if (!sm.OverlapCheck())
            {
                StartCoroutine(player.CantRotateAnimation());

                DirectionOfRay = sm.WallHitDuringRotation.transform.position - sm.transform.position;
                transform.rotation = Quaternion.Euler(0, player.currentRotation, 0);

                DirectionOfRay.y = 0;

                if (Physics.Raycast(sm.transform.position, DirectionOfRay, out Hit, LayersToHit))
                {
                    if (Hit.transform.CompareTag("Wall") || Hit.transform.CompareTag("SpecialTempWall"))
                    {
                        CantRotateHitWall = true;
                        Destroy(Instantiate(player.gamemanager.ui.CantRotateAnim, new Vector3(Hit.point.x, Hit.point.y + 0.7f, Hit.point.z), Quaternion.Euler(new Vector3(90, 0, 0))), 1);
                        PlayerController.isMoving = false;
                        PlayerController.stillRotating = false;
                    }
                }

                if (!CantRotateHitWall)
                {
                    DirectionOfRay = sm.WallHitDuringRotation.transform.position - transform.position;

                    DirectionOfRay.y = 0;

                    if (Physics.Raycast(transform.position, DirectionOfRay, out Hit, LayersToHit))
                    {
                        if (Hit.transform.CompareTag("Wall") || Hit.transform.CompareTag("SpecialTempWall"))
                        {
                            CantRotateHitWall = true;
                            Destroy(Instantiate(player.gamemanager.ui.CantRotateAnim, new Vector3(Hit.point.x, Hit.point.y + 0.7f, Hit.point.z), Quaternion.Euler(new Vector3(90, 0, 0))), 1);
                            PlayerController.isMoving = false;
                            PlayerController.stillRotating = false;
                        }
                    }
                }
                CantRotateHitWall = false;

                return;
            }
        }

        RotateShadowPlayer();
        foreach (ShadowFieldMagnet sm in ShadowMagnetsArray)
        {
            if (!sm.OverlapCheck())
            {
                StartCoroutine(player.CantRotateAnimation());

                DirectionOfRay = sm.WallHitDuringRotation.transform.position - sm.transform.position;
                transform.rotation = Quaternion.Euler(0, player.currentRotation, 0);

                DirectionOfRay.y = 0;

                if (Physics.Raycast(sm.transform.position, DirectionOfRay, out Hit, LayersToHit))
                {
                    if (Hit.transform.CompareTag("Wall") || Hit.transform.CompareTag("SpecialTempWall"))
                    {
                        CantRotateHitWall = true;
                        Destroy(Instantiate(player.gamemanager.ui.CantRotateAnim, new Vector3(Hit.point.x, Hit.point.y + 0.7f, Hit.point.z), Quaternion.Euler(new Vector3(90, 0, 0))), 1);
                        PlayerController.isMoving = false;
                        PlayerController.stillRotating = false;
                    }
                }

                if (!CantRotateHitWall)
                {
                    DirectionOfRay = sm.WallHitDuringRotation.transform.position - transform.position;

                    DirectionOfRay.y = 0;

                    if (Physics.Raycast(transform.position, DirectionOfRay, out Hit, LayersToHit))
                    {
                        if (Hit.transform.CompareTag("Wall") || Hit.transform.CompareTag("SpecialTempWall"))
                        {
                            CantRotateHitWall = true;
                            Destroy(Instantiate(player.gamemanager.ui.CantRotateAnim, new Vector3(Hit.point.x, Hit.point.y + 0.7f, Hit.point.z), Quaternion.Euler(new Vector3(90, 0, 0))), 1);
                            PlayerController.isMoving = false;
                            PlayerController.stillRotating = false;
                        }
                    }
                }
                CantRotateHitWall = false;

                return;
            }
        }

        transform.Rotate(Vector3.up * 30);
        foreach (ShadowFieldMagnet sm in ShadowMagnetsArray)
        {
            if (!sm.OverlapCheck())
            {
                StartCoroutine(player.CantRotateAnimation());

                DirectionOfRay = sm.WallHitDuringRotation.transform.position - sm.transform.position;
                transform.rotation = Quaternion.Euler(0, player.currentRotation, 0);

                DirectionOfRay.y = 0;

                if (Physics.Raycast(sm.transform.position, DirectionOfRay, out Hit, LayersToHit))
                {
                    if (Hit.transform.CompareTag("Wall") || Hit.transform.CompareTag("SpecialTempWall"))
                    {
                        CantRotateHitWall = true;
                        Destroy(Instantiate(player.gamemanager.ui.CantRotateAnim, new Vector3(Hit.point.x, Hit.point.y + 0.7f, Hit.point.z), Quaternion.Euler(new Vector3(90, 0, 0))), 1);
                        PlayerController.isMoving = false;
                        PlayerController.stillRotating = false;
                    }
                }

                if (!CantRotateHitWall)
                {
                    DirectionOfRay = sm.WallHitDuringRotation.transform.position - transform.position;

                    DirectionOfRay.y = 0;

                    if (Physics.Raycast(transform.position, DirectionOfRay, out Hit, LayersToHit))
                    {
                        if (Hit.transform.CompareTag("Wall") || Hit.transform.CompareTag("SpecialTempWall"))
                        {
                            CantRotateHitWall = true;
                            Destroy(Instantiate(player.gamemanager.ui.CantRotateAnim, new Vector3(Hit.point.x, Hit.point.y + 0.7f, Hit.point.z), Quaternion.Euler(new Vector3(90, 0, 0))), 1);
                            PlayerController.isMoving = false;
                            PlayerController.stillRotating = false;
                        }
                    }
                }
                CantRotateHitWall = false;

                return;
            }
        }

        player.RotatePlayer();

    }

    public void RotateShadowPlayer()
    {
        transform.Rotate(Vector3.up * 20);
    }

    public void FollowPlayer()
    {
        transform.position = player.transform.position;
    }
}

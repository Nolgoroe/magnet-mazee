﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCell : MonoBehaviour
{
    public bool isFullWithWall;
    public bool isFullWithMagnet;
    public bool IsFullWithPlayer;

    public Vector3 CellPos;

    private RaycastHit Hit;

    private LayerMask layerMask = 1 << 10;
    public LayerMask CellIgnore;

    public FindBlockToSpawn BlockManager;

    public void Init()
    {
        layerMask = ~layerMask;
        CellIgnore = ~CellIgnore;

        CellPos = transform.position;

        BlockManager = GameObject.Find("BlockSpawnManager").GetComponent<FindBlockToSpawn>();
    }

    private void Update()
    {

        //Debug.DrawRay(transform.position, Vector3.up);
        if (!PlayerController.isMoving)
        {
            if (Physics.Raycast(transform.position, Vector3.up, out Hit, 2f, CellIgnore))
            {
                //Debug.Log("1");
                if (Hit.transform.CompareTag("Wall") || Hit.transform.CompareTag("SpecialTempWall"))
                {
                    //Debug.Log("WTF");
                    isFullWithWall = true;
                    isFullWithMagnet = false;
                    IsFullWithPlayer = false;

                }
                else if (Hit.transform.CompareTag("Magnet"))
                {
                    //Debug.Log("WTF");
                    isFullWithMagnet = true;
                    isFullWithWall = false;
                    IsFullWithPlayer = false;

                }
                else if (Hit.transform.CompareTag("Player"))
                {
                    //Debug.Log("WTF");
                    IsFullWithPlayer = true;
                    isFullWithMagnet = false;
                    isFullWithWall = false;
                }
                else
                {
                    IsFullWithPlayer = false;
                    isFullWithMagnet = false;
                    isFullWithWall = false;
                }
            }
            else
            {
                IsFullWithPlayer = false;
                isFullWithMagnet = false;
                isFullWithWall = false;
            }
        }
        //}
    }

    public void SearchForTempWalls()
    {
        if (Physics.Raycast(transform.position, Vector3.up, out Hit, 2f, layerMask))
        {
            if (Hit.transform.CompareTag("TempWall") || Hit.transform.CompareTag("SpecialTempWall"))
            {
                if (!Hit.transform.GetComponent<WallData>().InArray)
                {
                    BlockManager.ShootRays(Hit.transform.GetComponent<WallData>(), transform.position);
                    //Debug.Log("Hiti");
                }
                else
                {
                    //Debug.Log("I'm already in array");
                }
            }
        }


    }
    //public void CheckFull()
    //{
    //    ShootRays = true;
    //    Invoke("DisableRays", 1f);
    //}

    //private void DisableRays()
    //{
    //    ShootRays = false;
    //}
}

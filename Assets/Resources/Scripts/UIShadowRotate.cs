﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIShadowRotate : MonoBehaviour
{
    public GameObject Player;

    public void RotateWithPlayer()
    {
        iTween.RotateAdd(gameObject, iTween.Hash("amount", new Vector3(0, 90f, 0), "space", Space.World, "time", 0.15f, "easetype", iTween.EaseType.easeInQuad));
    }
}

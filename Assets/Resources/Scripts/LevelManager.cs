﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{

    public GameObject floor;

    public List<LevelScriptableObject> LevelsArray;

    public List<GameObject> CurrentActiveLevel;

    public ColorToPrefab[] colorMappings;
    public Material[] levelMaterials;

    public List<GameObject> SpecialWalls;
    public List<GameObject> SpecialWallsInLevel;

    private GameManager gameManager;

    public GameObject CurrentDummyLevelSpawn;
    public GameObject CurrentLevelSpawn;

    GameObject TempBlockSpawned;

    public int CurrentLevel = 0; /// LATER SAVE AND READ THE LEVEL NUMBER FROM THE DATABASE
    public int CurrentMaterial = 0;

    private int RandomSpecialNum = 0 ;

    public static bool LevelStart = false;

    private bool ArrayDone = false;

    private Texture2D CurrentMap;
    public FindBlockToSpawn BlockManager;

    public static int NumOfTempWalls;

    public void Init()
    {
        CurrentMap = LevelsArray[CurrentLevel].map; /// LATER READ THE LEVEL FROM THE DATABASE
        gameManager = GameManager.instance;

        BlockManager = GameObject.Find("BlockSpawnManager").GetComponent<FindBlockToSpawn>();


        if (gameManager.ui.levelNumber == 14)
        {
            gameManager.ui.TapForNextLevel.gameObject.SetActive(false);
        }
        //Debug.Log("THE COUNT " + NewClass.Level.Count);
        //gameManager.InitGameManager();
        if (!LevelStart)
        {
            floor = Instantiate(floor, floor.transform.position, Quaternion.identity);
            floor.GetComponent<MeshRenderer>().material = LevelsArray[CurrentLevel].FloorMat;
            gameManager.ui.YouWinObject.color = LevelsArray[CurrentLevel].YouWinTextColor;
            gameManager.ui.TapForNextLevel.color = LevelsArray[CurrentLevel].TapForNextLevel;
            gameManager.ui.BackGround.sprite = LevelsArray[CurrentLevel].BackgroundSprite;
            gameManager.ui.CurrentLevelNumber.color = LevelsArray[CurrentLevel].LevelNameColor;
            gameManager.ui.SettingsSprite.color = LevelsArray[CurrentLevel].LevelSettingsColor;
            GenerateDummyLevel();
        }
        //Debug.Log("THE COUNT " + BlockManager.TheLevel.Count);
    }

    void GenerateDummyLevel()
    {
        CurrentActiveLevel.Clear();

        LevelStart = false;
        ArrayDone = false;

        NumOfTempWalls = 0;
        RandomSpecialNum = 0;

        Destroy(CurrentLevelSpawn);

        Destroy(CurrentDummyLevelSpawn);

        CurrentDummyLevelSpawn = new GameObject();

        CurrentDummyLevelSpawn.name = "DummyLevel";

        for (int x = 0; x < CurrentMap.width; x++)
        {
            for (int y = 0; y < CurrentMap.height; y++)
            {
                GenerateDummyTile(x, y);
            }
        }

        //Search();
        //StartCoroutine(WaitFunction());

        Invoke("WaitFunction", 0.001f);

        SpecialWallsInLevel.Clear();

        //Debug.Log("First Check " + SpecialWallsInLevel.Count);
        SpecialWallsInLevel.AddRange(GameObject.FindGameObjectsWithTag("SpecialTempWall"));

        //Debug.Log("Second Check " + SpecialWallsInLevel.Count);

        foreach (GameObject specialwall in SpecialWallsInLevel)
        {
            if(specialwall.GetComponent<WallData>().blocks == WallData.TypesOfBlocks.LeftRight)
            {
                specialwall.transform.rotation = Quaternion.Euler(0, 90f, 0);
            }

            if(specialwall.GetComponent<WallData>().blocks == WallData.TypesOfBlocks.TopDown)
            {
                specialwall.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }

        SpecialWallsInLevel.Clear();
    }

    public void GenerateDummyTile(int posX, int posZ)
    {

        Color pixelColor = CurrentMap.GetPixel(posX, posZ);

        //if(posX == 0 && posZ == 6)
        //{
        //    Debug.Log("The Color is: " + pixelColor);
        //}

        if (pixelColor.a < 0.5f)
        {
            return;
        }

        pixelColor.a = 1;

        //if (pixelColor.r < 0.1f && pixelColor.g == 1)
        //{
        //    Debug.Log("The Color is: " + posX + "/" + posZ + "/" + pixelColor);
        //}

        foreach (ColorToPrefab colorMapping in colorMappings)
        {
            //Debug.Log(pixelColor + " / " + colorMapping.color);
            if (colorMapping.color.Equals(pixelColor))
            {
                //Debug.Log("THIS" + pixelColor);
                Vector3 position = new Vector3(posX-3f, 0.6f, posZ-3f);

                if (colorMapping.prefab.transform.CompareTag("TempWall"))
                {
                    TempBlockSpawned = Instantiate(colorMapping.prefab, position, Quaternion.Euler(colorMapping.prefab.transform.eulerAngles), CurrentDummyLevelSpawn.transform);
                    TempBlockSpawned.GetComponent<WallData>().Position = position;
                    TempBlockSpawned.GetComponent<WallData>().WallColor = pixelColor;

                    if (pixelColor == Color.black)
                    {
                        TempBlockSpawned.gameObject.layer = 8;
                    }

                    if (pixelColor == Color.white)
                    {
                        TempBlockSpawned.gameObject.layer = 9;
                    }

                    break;
                }

                if (colorMapping.prefab.transform.CompareTag("SpecialTempWall"))
                {
                    RandomSpecialNum = UnityEngine.Random.Range(0, SpecialWalls.Count);
                    GameObject SpecialBlock = Instantiate(SpecialWalls[RandomSpecialNum], new Vector3(position.x, 0.1f, position.z), Quaternion.Euler(SpecialWalls[RandomSpecialNum].transform.eulerAngles), CurrentDummyLevelSpawn.transform);
                    SpecialBlock.GetComponent<WallData>().Position = new Vector3(position.x, 0.1f, position.z);
                    SpecialBlock.GetComponent<WallData>().WallColor = pixelColor;

                    if(pixelColor == Color.black)
                    {
                        SpecialBlock.gameObject.layer = 8;
                    }

                    if(pixelColor == Color.white)
                    {
                        SpecialBlock.gameObject.layer = 9;
                    }

                    break;
                }
            }
        }

        //Search(pixelColor);

        //StartCoroutine(Search(pixelColor));
        //Invoke("Search", 0.005f);
    }

    public void GenerateActualLevel()
    {
        CurrentActiveLevel.Clear();

        SpecialWallsInLevel.Clear();
        //Debug.Log("Third Check " + SpecialWallsInLevel.Count);

        //Debug.Log("THE COUNT " + BlockManager.TheLevel.Count);
        //Debug.Log("In Here");

        Destroy(CurrentLevelSpawn);

        CurrentLevelSpawn = new GameObject();

        CurrentLevelSpawn.name = "Real Level";

        Destroy(CurrentDummyLevelSpawn);

        //Debug.Log("THE COUNT " + BlockManager.TheLevel.Count);

        for (int i = 0; i < BlockManager.TheLevel.Count; i++)
        {
            Vector3 position = BlockManager.TheLevel[i].GetComponent<WallData>().Position;
            GameObject BlocksToSpawn = Instantiate(BlockManager.TheLevel[i].Prefab, new Vector3(position.x, 0.1f, position.z), BlockManager.TheLevel[i].Prefab.transform.rotation, CurrentLevelSpawn.transform);
            CurrentActiveLevel.Add(BlocksToSpawn);
            BlocksToSpawn.GetComponent<MeshRenderer>().material = levelMaterials[CurrentMaterial];
        }

        floor.GetComponent<MeshRenderer>().material = LevelsArray[CurrentLevel].FloorMat;

        for (int x = 0; x < CurrentMap.width; x++)
        {
            for (int y = 0; y < CurrentMap.height; y++)
            {
                GenerateTile(x, y);
            }
        }

        //Debug.Log("Fourth Check " + SpecialWallsInLevel.Count);

        LevelStart = true;
        gameManager.InitGameManager();

        //Debug.Log("Fifth Check " + SpecialWallsInLevel.Count);

    }

    void GenerateTile(int posX, int posZ)
    {
        Color pixelColor = CurrentMap.GetPixel(posX, posZ);

        if (pixelColor.a < 0.5f)
        {
            return;
        }

        pixelColor.a = 1;

        foreach (ColorToPrefab colorMapping in colorMappings)
        {
            if (colorMapping.color.Equals(pixelColor) && pixelColor != Color.black)
            {
                Vector3 position = new Vector3(posX - 3f, 0f, posZ - 3f);

                if (colorMapping.prefab.transform.CompareTag("Magnet"))
                {
                    Instantiate(colorMapping.prefab, new Vector3(position.x, 0.55F, position.z), colorMapping.prefab.transform.rotation, CurrentLevelSpawn.transform);
                }
                else if(colorMapping.prefab.transform.CompareTag("Player"))
                {
                    //if (gameManager.Player)
                    //{
                    //    gameManager.Player.transform.position = new Vector3(position.x, 20, position.z);
                    //}
                    //else
                    //{
                        Instantiate(colorMapping.prefab, new Vector3(position.x, 20, position.z), colorMapping.prefab.transform.rotation); /// NO PARENT
                    //}
                }
            }
        }
    }

    public IEnumerator LoadNextLevel()
    {
        if(CurrentLevel != 14)
        {
            //gameManager.Player.transform.position = new Vector3(100, 20, 100);
            yield return new WaitForEndOfFrame();
            List<GameObject> cantRotateObjects = new List<GameObject>();
            cantRotateObjects.AddRange(GameObject.FindGameObjectsWithTag("cant rotate"));

            //gameManager.Player.shadowPlayer.transform.rotation = Quaternion.identity;
            foreach (GameObject cant in cantRotateObjects)
            {
                Destroy(cant.gameObject);
            }

            if ((!ManageEndAnimation.DestroyedAll))
            {
                ManageEndAnimation.DestroyedAll = true;
                Destroy(gameManager.EndAnimManager.PlayerEndAnimationMagnet.transform.root.gameObject);

                Destroy(GameObject.FindGameObjectWithTag("Shadow Player").gameObject);
            }

            foreach (GameObject EndAnimFieldMag in GameObject.FindGameObjectsWithTag("EndAnimFieldMagnet"))
            {
                Destroy(EndAnimFieldMag.gameObject);
            }

            //if ((gameManager.EndAnimManager.PlayerEndAnimationMagnet.transform.root.gameObject))
            //{
            //    Destroy(gameManager.EndAnimManager.PlayerEndAnimationMagnet.transform.root.gameObject);

            //    Destroy(GameObject.FindGameObjectWithTag("Shadow Player").gameObject);
            //}

            CurrentLevel++;
            CurrentMaterial++;

            CurrentMap = LevelsArray[CurrentLevel].map; /// LATER READ THE LEVEL FROM THE DATABASE
            gameManager.ui.YouWinObject.color = LevelsArray[CurrentLevel].YouWinTextColor;
            gameManager.ui.TapForNextLevel.color = LevelsArray[CurrentLevel].TapForNextLevel;
            gameManager.ui.BackGround.sprite = LevelsArray[CurrentLevel].BackgroundSprite;
            gameManager.ui.CurrentLevelNumber.color = LevelsArray[CurrentLevel].LevelNameColor;
            gameManager.ui.SettingsSprite.color = LevelsArray[CurrentLevel].LevelSettingsColor;
            GenerateDummyLevel();

        }
    }

    public void RestartLevel()
    {
        PlayerWinAnimation.SpeedUpExplosion = false;
        if ((!ManageEndAnimation.DestroyedAll))
        {
            ManageEndAnimation.DestroyedAll = true;
            Destroy(gameManager.EndAnimManager.PlayerEndAnimationMagnet.transform.root.gameObject);

            Destroy(GameObject.FindGameObjectWithTag("Shadow Player").gameObject);

        }

        foreach (GameObject EndAnimFieldMag in GameObject.FindGameObjectsWithTag("EndAnimFieldMagnet"))
        {
            Destroy(EndAnimFieldMag.gameObject);
        }

        List<GameObject> cantRotateObjects = new List<GameObject>();
        cantRotateObjects.AddRange(GameObject.FindGameObjectsWithTag("cant rotate"));

        //gameManager.Player.shadowPlayer.transform.rotation = Quaternion.identity;

        foreach (GameObject cant in cantRotateObjects)
        {
            Destroy(cant.gameObject);
        }


        CurrentMap = LevelsArray[CurrentLevel].map; /// LATER READ THE LEVEL FROM THE DATABASE

        UIManager.RestartingLevel = false;

        GenerateDummyLevel();

    }

    public void Search()
    {
        //Debug.Log("In Here Searching");

        //yield return new WaitForFixedUpdate();

        foreach (GridCell Cell in gameManager.GridManager.CellsInWorld)
        {
            Cell.SearchForTempWalls();
        }
    }

    //IEnumerator WaitFunction()
    //{

    //    yield return new WaitForSeconds(0.001f);

    //    Search();

    //    BlockManager.CheckArrayDone();

    //    for (int i = 0; i < CurrentActiveLevel.Count; i++)
    //    {
    //        if (CurrentActiveLevel[i].CompareTag("SpecialTempWall") && CurrentActiveLevel[i].GetComponent<WallData>().blocks == WallData.TypesOfBlocks.NoSides)
    //        {
    //            SpecialWallsInLevel.Add(CurrentActiveLevel[i].gameObject);
    //        }
    //    }

    //    yield return null;
    //}

    void WaitFunction()
    {
        Search();

        BlockManager.CheckArrayDone();

        for (int i = 0; i < CurrentActiveLevel.Count; i++)
        {
            if (CurrentActiveLevel[i].CompareTag("SpecialTempWall") && CurrentActiveLevel[i].GetComponent<WallData>().blocks == WallData.TypesOfBlocks.NoSides)
            {
                SpecialWallsInLevel.Add(CurrentActiveLevel[i].gameObject);
            }
        }
    }
}

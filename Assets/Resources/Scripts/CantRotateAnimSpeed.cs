﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CantRotateAnimSpeed : MonoBehaviour
{
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        //anim["Cant_Rotate_ANIM"].speed = 2;
        anim.speed = 2;
    }
}

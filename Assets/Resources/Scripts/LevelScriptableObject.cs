﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "CreateLevel")]
public class LevelScriptableObject : ScriptableObject
{
    public Texture2D map;

    public Material FloorMat;

    public Sprite BackgroundSprite;

    public Color LevelNameColor;

    public Color LevelSettingsColor;

    public Color YouWinTextColor;

    public Color TapForNextLevel;
}

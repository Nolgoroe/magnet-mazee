﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallData : MonoBehaviour
{
    public bool[] bools = { false, false, false, false};
    public enum TypesOfBlocks { Top, Right, Left, Down, TopDown, LeftRight, LeftDown, LeftUp, RightDown, RightUp, LeftUpRight, LeftDownRight, UpRightDown, LeftUpDown, AllSIdes, NoSides }
    public TypesOfBlocks blocks = TypesOfBlocks.NoSides;

    public bool InArray = false;

    public Vector3 Position;

    public GameObject Prefab;

    public Color WallColor;
}

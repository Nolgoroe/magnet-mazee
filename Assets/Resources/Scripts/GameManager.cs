﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singelton

    public static GameManager instance;

    private void Awake()
    {
        instance = this;
    }


    #endregion

    public Camera MainCam;

    public Grid GridManager;
    public LevelManager levelgenerator;
    public Magnets MagnetManager;
    public UIManager ui;
    public FindBlockToSpawn BlockSpawnOrderManager;
    public PlayerController Player;
    public SoundManager SoundManager;
    public ManageEndAnimation EndAnimManager;

    public List<FieldMagnet> MagnetsOnField;

    public List<GameObject> AllLevelMagnets;

    public static bool LevelWon = false;

    public GameObject SpeedUpEndLevelAnimButton;

    public void Start()
    {
        SpeedUpEndLevelAnimButton.SetActive(false);

        MainCam.gameObject.SetActive(false);
        //if (gamemanager.AllLevelMagnets.Count == 0) /// THIS WILL GO AFTER TAP FOR NEXT LEVEL IN UI MANAGER
        //{
        //    gamemanager.levelgenerator
        //}


        if (LevelManager.LevelStart)
        {
            AllLevelMagnets = new List<GameObject>();
            //AllLevelMagnets.Clear();
            AllLevelMagnets.AddRange(GameObject.FindGameObjectsWithTag("Magnet"));

            MagnetsOnField = new List<FieldMagnet>();
            //MagnetsOnField.Clear();
        }


        GridManager.Init();

        foreach (GridCell Cell in GridManager.CellsInWorld)
        {
            Cell.Init();
        }


        BlockSpawnOrderManager.Init();

        levelgenerator.Init();

        ui.Init();


        //Debug.Log("THE COUNT IN GAMEMANAGER " + BlockSpawnOrderManager.TheLevel.Count);

        MagnetManager.Init();


        if (LevelManager.LevelStart)
        {
            Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

            Player.InitPlayer();

            foreach (GameObject FieldMagnets in GameObject.FindGameObjectsWithTag("Magnet"))
            {
                if (FieldMagnets.CompareTag("Magnet"))
                {
                    MagnetsOnField.Add(FieldMagnets.GetComponent<FieldMagnet>());
                }
            }

            foreach (FieldMagnet Magnet in MagnetsOnField)
            {
                Magnet.InitMagnets();
            }

            MagnetsOnField.Clear();

        }
    }

    public void InitGameManager()
    {
        SpeedUpEndLevelAnimButton.SetActive(false);
        LevelWon = false;
        if (LevelManager.LevelStart)
        {
            Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

            Player.InitPlayer();

            AllLevelMagnets.Clear();

            MagnetsOnField.Clear();

            Invoke("FillMagnetArrays", 0.005f);
        }

        GridManager.Init();

        BlockSpawnOrderManager.Init();

        levelgenerator.Init();

        ui.Init();

        MagnetManager.Init();


        foreach (GameObject FieldMagnets in GameObject.FindGameObjectsWithTag("Magnet"))
        {
            if (FieldMagnets.CompareTag("Magnet"))
            {
                MagnetsOnField.Add(FieldMagnets.GetComponent<FieldMagnet>());
            }
        }

        foreach (FieldMagnet Magnet in MagnetsOnField)
        {
            Magnet.InitMagnets();
        }

        MagnetsOnField.Clear();

        EndAnimManager.Init();

        MainCam.gameObject.SetActive(true);

    }

    public void CheckAmountOfMagnets()
    {
        if(AllLevelMagnets.Count == 0)
        {
            PlayerController.CanMove = false;
            LevelWon = true;
            //ui.EnableWinScreen();1234
        } 
    }

    public void FillMagnetArrays()
    {
        if (LevelManager.LevelStart)
        {
            //AllLevelMagnets.Clear();
            AllLevelMagnets.AddRange(GameObject.FindGameObjectsWithTag("Magnet"));

            //MagnetsOnField = new List<FieldMagnet>();

            foreach (GameObject FieldMagnets in GameObject.FindGameObjectsWithTag("Magnet"))
            {
                if (FieldMagnets.CompareTag("Magnet"))
                {
                    MagnetsOnField.Add(FieldMagnets.GetComponent<FieldMagnet>());
                }
            }

            foreach (FieldMagnet Magnet in MagnetsOnField)
            {
                Magnet.InitMagnets();
            }

        }
    }

    public IEnumerator SpeedUpEndAnimation()
    {
        yield return new WaitForSeconds(0.1f);

        ui.EnableWinScreen();

        yield return null;
    }

    public void SpeedUpEndAnim()
    {
        PlayerWinAnimation.SpeedUpExplosion = true;
        Debug.Log("WIN SCREEN");
        StartCoroutine(SpeedUpEndAnimation());
    }
}

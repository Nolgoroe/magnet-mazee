﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip[] audioClips;

    public AudioClip[] SmallMagTicks;

    public static AudioSource Audiosource;


    private void Start()
    {
        Audiosource = GetComponent<AudioSource>();
    }

    public void PlaySound(int Index)
    {
        Audiosource.PlayOneShot(audioClips[Index]);
    }

    public void PlaySmallMagTicks()
    {
        
        int index = Random.Range(1, SmallMagTicks.Length);

        //Debug.Log(index);

        Audiosource.PlayOneShot(SmallMagTicks[index]);
    }

    public void StopAllSounds()
    {
        if (Audiosource.isPlaying)
        {
            Audiosource.Stop();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldMagnetsEndAnimation : MonoBehaviour
{
    public GameObject PlayerEndAnimMagnet;

    public float MaxForce = 15;

    public float Multi = 4;

    float FixedScale = 1;

    bool isChild = false;

    public bool EndAnimMagnetized = false;

    GameManager gamemanager;
    //public Animator Anim;

    private void Start()
    {
        EndAnimMagnetized = false;
        //Anim = transform.parent.GetComponent<Animator>();
        PlayerEndAnimMagnet = GameObject.FindGameObjectWithTag("EndAnimPlayer");
        gamemanager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void Update()
    {
        //if (GameManager.LevelWon)
        //{
        //    transform.GetComponent<Rigidbody>().isKinematic = false;
        //}  

    }

    public void EndLevelMagnetization()
    {
        //Debug.Log("I'M IN HERE YOHOOO");
        //Anim.SetBool("RattleNow", true);

        //if (Anim.GetCurrentAnimatorStateInfo(0).IsName("Rattle"))
        //{

        //if (!isChild)
        //{
        float forceMultiplier = MaxForce / Vector3.Distance(transform.position, PlayerEndAnimMagnet.transform.position);

        if(forceMultiplier < 2)
        {
            forceMultiplier = 2;
        }

        //Debug.Log(forceMultiplier);
        //Debug.Log(Vector3.Distance(transform.position, PlayerEndAnimMagnet.transform.position));

        transform.GetComponent<Rigidbody>().AddForce((PlayerEndAnimMagnet.transform.position - transform.position).normalized * (forceMultiplier * forceMultiplier) * Multi, ForceMode.Force);
        //}

        if (isChild && !PlayerWinAnimation.FinishedMagnetizing)
        {
            transform.localScale = new Vector3(FixedScale / transform.parent.localScale.x, FixedScale / transform.parent.localScale.y, FixedScale / transform.parent.localScale.z);
        }
        //}
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("EndAnimPlayer") && PlayerWinAnimation.StartMagnetizing && !isChild)
        {
            //Debug.Log("Player");
            gamemanager.SoundManager.PlaySmallMagTicks();
            transform.SetParent(collision.transform);
            isChild = true;
            transform.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.transform.CompareTag("EndAnimPlayer") && !isChild && PlayerWinAnimation.PullForceOn)
    //    {
    //        //Debug.Log("Player");
    //        transform.SetParent(other.transform);
    //        isChild = true;
    //        transform.GetComponent<Rigidbody>().isKinematic = true;
    //    }
    //}
}

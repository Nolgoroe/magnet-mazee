﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWinAnimation : MonoBehaviour
{
    public float radius;

    public GameObject Target;

    public float IncaseTimerTillExplosion = 2f;

    //public GameObject Manager;

    public Vector3 TargetMaxScale;

    public float RadiusIncreaseScaleSpeed = 2f;
    public float TargetScalseSizeSpeed = 10f;
    public float RadiusMaxLimit = 10f;
    public float RotationSpeed = 0.1f;
    public float RotationSpeedMod = 0.1f;
    public float GoUpSpeed = 0.1f;

    private Collider[] ObjectsInSphere;
    public List<FieldMagnetsEndAnimation> MagnetizedMags;
    public static bool FinishedMagnetizing = false;
    public static bool StartMagnetizing = false;
    public static bool PullForceOn = false;
    public static bool StartLights = false;

    public static bool PreventRotation;

    public static bool SpeedUpExplosion;

    GameManager gamemanager;

    bool StartedMoving = false;
    bool PlaySoundOnce = true;

    public void Init()
    {
        PlaySoundOnce = true;
        StartLights = false;
        SpeedUpExplosion = false;
        PreventRotation = false;
        PullForceOn = false;
        StartMagnetizing = false;
        FinishedMagnetizing = false;
        MagnetizedMags.Clear();
        MagnetizedMags = new List<FieldMagnetsEndAnimation>();
        gamemanager = GameManager.instance;
    }

    void FixedUpdate()
    {
        if (GameManager.LevelWon)
        {
            gamemanager.SpeedUpEndLevelAnimButton.SetActive(true);
            Invoke("TurnOnEndLevelLights", 0.3f);

            PreventRotation = true;

            IncaseTimerTillExplosion -= Time.deltaTime;

            Invoke("TurnOnPullForce", 1.7f);
            //Debug.Log("AMOUNT IS: " + ((float)((gamemanager.MagnetsOnField.Count + 1) * 9) - 1));

            if (Target.transform.childCount == ((float)((gamemanager.MagnetsOnField.Count + 1) * 9) - 1)|| IncaseTimerTillExplosion <= 0 || Input.touchCount > 0)
            {
                //FinishedMagnetizing = true;
                gamemanager.GetComponent<ManageEndAnimation>().incereasesizes();
                StartCoroutine(gamemanager.GetComponent<ManageEndAnimation>().Exploade());
            }

            Invoke("MoveCenterPlayerMag", 0.9f);

            if (!FinishedMagnetizing && StartedMoving)
            {
                RotationSpeed += Time.deltaTime * RotationSpeedMod;
              
                radius = GetComponent<SphereCollider>().radius;

                if (radius < RadiusMaxLimit)
                {
                    radius += RadiusIncreaseScaleSpeed * Time.deltaTime;
                }

                GetComponent<SphereCollider>().radius = radius;

                ObjectsInSphere = Physics.OverlapSphere(transform.position, radius);

                foreach (Collider Mag in ObjectsInSphere)
                {
                    if (Mag.CompareTag("EndAnimFieldMagnet") && StartMagnetizing)
                    {
                        Mag.transform.GetComponent<Rigidbody>().isKinematic = false;

                        Mag.GetComponent<FieldMagnetsEndAnimation>().EndLevelMagnetization();

                        //Debug.Log("I'm in");
                        if (!Mag.GetComponent<FieldMagnetsEndAnimation>().EndAnimMagnetized)
                        {
                            Mag.GetComponent<FieldMagnetsEndAnimation>().EndAnimMagnetized = true;
                            MagnetizedMags.Add(Mag.GetComponent<FieldMagnetsEndAnimation>());
                        }
                    }
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, radius);
    }

    public void TurnOnPullForce()
    {
        PullForceOn = true;
        StartMagnetizing = true;

        foreach (GameObject ShadowSilhouette in GameObject.FindGameObjectsWithTag("Shadow silhouette"))
        {
            ShadowSilhouette.SetActive(false);
        }

    }

    public void MoveCenterPlayerMag()
    {
        StartLights = false;
        transform.position = Vector3.Lerp(transform.position, new Vector3(4.5f, 4.5f, 3.5f), GoUpSpeed * Time.deltaTime);

        Target.transform.localScale = Vector3.Lerp(Target.transform.localScale, TargetMaxScale, TargetScalseSizeSpeed * Time.deltaTime);

        transform.Rotate(new Vector3(Time.deltaTime * RotationSpeed, 0, 0));

        StartedMoving = true;

        if (PlaySoundOnce)
        {
            PlaySoundOnce = false;
            //gamemanager.SoundManager.PlaySound(9);
        }
    }

    public void TurnOnEndLevelLights()
    {
        StartLights = true;
    }

    public void CheckUiOn()
    {
        if (!gamemanager.ui.PopUp.activeInHierarchy)
        {
            Debug.Log("Sped Up");
            //gamemanager.SpeedUpEndAnim();
        }
        else
        {
            Debug.Log("UI on");
        }
    }
}


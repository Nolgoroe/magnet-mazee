﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : Magnets
{
    int MoveX, MoveZ;

    public GameObject MoveParticalFX;
    public UIShadowRotate PlayerShadowUI;

    public PlayerWinAnimation PlayerEndLevelAnimControl;

    public float speed;
    public int currentRotation;
    //public int tapThreshHold;

    public static bool stillRotating;
    public static bool isMoving;

    public Touch touch;

    //public Vector3 movingDir;
    //public Vector3 TargetMovePosition;

    public GameObject shadowPlayer;

    public static List<Magnets> PlayerMagnetsArray = new List<Magnets>();

    public static ParticleSystem[] PlayerParticleSystems;

    public List<Magnets> CheckingList;
    public GameManager gamemanager;
    public Animation anim;
    public Vibration vib;

    private GameObject vibrationManager;

    private Vector2 touchStartPos;
    private Vector2 touchEndPos;
    private Vector2 currentSwipeProgress;
    private Vector2 SwipingCurrentPos;

    private bool OneTouch;
    private bool PlayLightSoundOnce = true;

    public bool testbool;
    public static bool CollidingWithWall, CollidingWithMagnet;
    public static bool CanMove;

    public float GeneralCounter;
    public Light EndLevelLight;
    public float LightMaxIntensityUP;
    public float LightMaxIntensityDOWN;

    private void Start()
    {
        shadowPlayer = Instantiate(shadowPlayer, transform.parent);
    }

    public void InitPlayer()
    {
        PlayLightSoundOnce = true;
        EndLevelLight.gameObject.SetActive(false);
        //MoveParticalFX.SetActive(false);

        for (int i = 0; i < transform.GetChild(1).transform.childCount; i++)
        {
            if (transform.GetChild(1).transform.GetChild(i).CompareTag("Magnet"))
            {
                Destroy(transform.GetChild(1).transform.GetChild(i).gameObject);
            }
        }

        PlayerParticleSystems = GetComponentsInChildren<ParticleSystem>();

        //if (transform.GetChild(0).transform.childCount > 0)
        //{
        //    for (int i = 0; i < transform.GetChild(0).transform.childCount; i++)
        //    {
        //        Destroy(transform.GetChild(0).transform.GetChild(i).gameObject);
        //    }
        //}

        //if (shadowPlayer.transform.childCount > 0)
        //{
        //    for (int i = 0; i < shadowPlayer.transform.childCount; i++)
        //    {
        //        Destroy(shadowPlayer.transform.GetChild(i).gameObject);
        //    }
        //}

        currentRotation = 0;
        PlayerMagnetsArray = new List<Magnets>();
        shadowPlayer.GetComponent<ShadowPlayer>().ShadowMagnetsArray = new List<ShadowFieldMagnet>();
        vibrationManager = GameObject.Find("Vibration Manager");
        vib = vibrationManager.GetComponent<Vibration>();
        anim = GetComponent<Animation>();
        PlayerMagnetsArray.Add(this);

        CheckingList = PlayerMagnetsArray;
        GetCurrentCell(transform.position, gameObject, true);
        stillRotating = false;
        OneTouch = false;
        CollidingWithWall = false;
        CollidingWithMagnet = false;
        GravityFall();

        PlayerEndLevelAnimControl.Init();

        gamemanager = GameManager.instance;
    }

    private void Update()
    {
        testbool = isMoving;
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                touchStartPos = touch.position;

                currentSwipeProgress = Vector2.zero;
                SwipingCurrentPos = Vector2.zero;
                //TargetMovePosition = Vector3.zero;
                OneTouch = true;
            }

            if (touch.phase == TouchPhase.Moved)
            {
                SwipingCurrentPos = new Vector2(touch.position.x, touch.position.y);

                currentSwipeProgress = SwipingCurrentPos - touchStartPos;

                //Debug.Log(currentSwipeProgress.sqrMagnitude);
                //Debug.Log(stillRotating);
                //Debug.Log(OneTouch);
                if (currentSwipeProgress.sqrMagnitude > 0.05f * Screen.width && !stillRotating && OneTouch)
                {
                    OneTouch = false;
                    currentSwipeProgress.Normalize();

                    //if(TargetMovePosition != Vector3.zero)
                    //{
                    //    StartCoroutine(MovementHandle());
                    //}
                    if (CanMove)
                    {
                        if (!isMoving && !IsFalling && !gamemanager.ui.PopUp.activeInHierarchy && !gamemanager.ui.winScreen.activeInHierarchy)
                        {
                            SetDestination();

                            if (MoveX != 0)
                            {
                                //Debug.Log("Moved X " + MoveX);
                                StartCoroutine(MovementHandleX());
                            }
                            if (MoveZ != 0)
                            {
                                //Debug.Log("Moved Z");
                                StartCoroutine(MovementHandleZ());
                            }
                        }
                    }
                }
            }

            if (touch.phase == TouchPhase.Ended)
            {
                if (!gamemanager.ui.uiTouch)
                {
                    //Debug.Log((touchStartPos - touchEndPos).magnitude);
                    touchEndPos = touch.position;
                    currentSwipeProgress = Vector2.zero;

                    if ((touchStartPos - touchEndPos).magnitude < 0.01f * Screen.width && PlayerMagnetsArray.Count > 1 /*&& !ButtonManager.Levelwon WE NEED THIS LATER DO NOT DELETE*/)
                    {
                        if (!IsFalling && !stillRotating && !isMoving && !gamemanager.ui.winScreen.activeInHierarchy && !gamemanager.ui.PopUp.activeInHierarchy && !PlayerWinAnimation.PreventRotation)
                        {
                            stillRotating = true;
                            shadowPlayer.GetComponent<ShadowPlayer>().RotateShadowPlayerControl();
                        }
                    }
                }
            }
        }

        if (PlayerWinAnimation.StartLights)
        {
            GeneralCounter += Time.deltaTime;

            foreach (Magnets Mag in PlayerMagnetsArray)
            {
                if(GeneralCounter < 0.2f)
                {
                    if (Mag.CompareTag("Player"))
                    {
                        Mag.GetComponent<PlayerController>().EndLevelLight.gameObject.SetActive(true);

                        Mag.GetComponent<PlayerController>().EndLevelLight.intensity += LightMaxIntensityUP * Time.deltaTime;
                        //StartCoroutine(gamemanager.Player.TurnOnEndLevelLight(Mag.GetComponent<PlayerController>().EndLevelLight, 0.5f));
                    }

                    if (Mag.CompareTag("Magnet"))
                    {
                        Mag.GetComponent<FieldMagnet>().EndLevelLight.gameObject.SetActive(true);
                        Mag.GetComponent<FieldMagnet>().EndLevelLight.intensity += LightMaxIntensityUP * Time.deltaTime;
                        //StartCoroutine(gamemanager.Player.TurnOnEndLevelLight(Mag.GetComponent<FieldMagnet>().EndLevelLight, 0.5f));
                    }
                }
                else if(GeneralCounter > 0)
                {
                    if (Mag.CompareTag("Player"))
                    {
                        Mag.GetComponent<PlayerController>().EndLevelLight.gameObject.SetActive(true);

                        Mag.GetComponent<PlayerController>().EndLevelLight.intensity -= LightMaxIntensityDOWN * Time.deltaTime;
                        //StartCoroutine(gamemanager.Player.TurnOnEndLevelLight(Mag.GetComponent<PlayerController>().EndLevelLight, 0.5f));
                    }

                    if (Mag.CompareTag("Magnet"))
                    {
                        Mag.GetComponent<FieldMagnet>().EndLevelLight.gameObject.SetActive(true);
                        Mag.GetComponent<FieldMagnet>().EndLevelLight.intensity -= LightMaxIntensityDOWN * Time.deltaTime;
                        //StartCoroutine(gamemanager.Player.TurnOnEndLevelLight(Mag.GetComponent<FieldMagnet>().EndLevelLight, 0.5f));
                    }
                }
            }

            if (PlayLightSoundOnce)
            {
                PlayLightSoundOnce = false;
                Invoke("PlayLightSound", 0.07f);
            }
        }
    }

    //IEnumerator MovementHandle()
    //{
    //    Debug.Log("Moving");
    //    isMoving = true;
    //    iTween.MoveTo(gameObject, iTween.Hash("position", TargetMovePosition, "speed", speed, "easeType", iTween.EaseType.easeInOutExpo, "oncomplete", "ResetMovement"));
    //    yield return null;
    //}

    IEnumerator MovementHandleX()
    {
        //Debug.Log("moveing X times Player");
        //Debug.Log("Moving");
        isMoving = true;

        gamemanager.ui.SwiptoMove.SetActive(false);

        //if (gamemanager.levelgenerator.CurrentLevel == 3 && CheckingList.Count > 1 && isMoving)
        //{
        //    gamemanager.ui.TaptoRotate.SetActive(false);
        //}

        if(MoveX > 0)
        {
            MoveParticalFX.transform.rotation = Quaternion.Euler(new Vector3(0, 270, 0));

            //MoveParticalFX.SetActive(true);

            foreach (ParticleSystem Parti in PlayerParticleSystems)
            {
                Parti.Play();
            }

            //MoveParticalFX.GetComponentInChildren<ParticleSystem>().Play();
        }

        if (MoveX < 0)
        {
            MoveParticalFX.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));

            //MoveParticalFX.SetActive(true);

            foreach (ParticleSystem Parti in PlayerParticleSystems)
            {
                Parti.Play();
            }

            //MoveParticalFX.GetComponentInChildren<ParticleSystem>().Play();
        }

        if (CollidingWithWall)
        {
            iTween.MoveBy(gameObject, iTween.Hash("X", MoveX, "space", Space.World, "speed", speed, "easeType", iTween.EaseType.easeInQuad, "oncomplete", "ResetMovement"));
        }

        if (CollidingWithMagnet)
        {
            iTween.MoveBy(gameObject, iTween.Hash("X", MoveX, "space", Space.World, "speed", speed, "easeType", iTween.EaseType.easeInOutExpo, "oncomplete", "ResetMovement"));
        }
        yield return new WaitForSeconds(0.1f);
        gamemanager.SoundManager.PlaySound(3);
        yield return null;
    }

    IEnumerator MovementHandleZ()
    {
        //Debug.Log("moveing X times Player");
        //Debug.Log("Moving");
        isMoving = true;

        gamemanager.ui.SwiptoMove.SetActive(false);

        if (MoveZ > 0)
        {
            //MoveParticalFX.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));

            //MoveParticalFX.SetActive(true);

            MoveParticalFX.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));

            foreach (ParticleSystem Parti in PlayerParticleSystems)
            {
                Parti.Play();
            }

            //MoveParticalFX.GetComponentInChildren<ParticleSystem>().Play();
        }

        if (MoveZ < 0)
        {
            MoveParticalFX.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));

            //MoveParticalFX.SetActive(true);

            foreach (ParticleSystem Parti in PlayerParticleSystems)
            {
                Parti.Play();
            }

            //MoveParticalFX.GetComponentInChildren<ParticleSystem>().Play();
        }


        if (CollidingWithWall)
        {
            iTween.MoveBy(gameObject, iTween.Hash("Z", MoveZ, "space", Space.World, "speed", speed, "easeType", iTween.EaseType.easeInQuad, "oncomplete", "ResetMovement"));
        }

        if (CollidingWithMagnet)
        {
            iTween.MoveBy(gameObject, iTween.Hash("Z", MoveZ, "space", Space.World, "speed", speed, "easeType", iTween.EaseType.easeInOutExpo, "oncomplete", "ResetMovement"));
        }

        yield return new WaitForSeconds(0.1f);
        gamemanager.SoundManager.PlaySound(3);
        yield return null;
    }

    public void ResetMovement()
    {
        //GetCurrentCell(new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y), Mathf.RoundToInt(transform.position.z)), gameObject);
        //Debug.Log("DONE Moving");
        //MoveParticalFX.SetActive(false);

        foreach (ParticleSystem Parti in PlayerParticleSystems)
        {
            Parti.Stop();
        }

        //MoveParticalFX.GetComponentInChildren<ParticleSystem>().Stop();
        IsFalling = false;
        MoveX = 0;
        MoveZ = 0;
        CollidingWithWall = false;
        CollidingWithMagnet = false;

        for (int i = 0; i < PlayerMagnetsArray.Count; i++)
        {
            GetCurrentCell(new Vector3(Mathf.RoundToInt(PlayerMagnetsArray[i].transform.position.x), Mathf.RoundToInt(PlayerMagnetsArray[i].transform.position.y), Mathf.RoundToInt(PlayerMagnetsArray[i].transform.position.z)), PlayerMagnetsArray[i].gameObject, false);
        }

        isMoving = false;

        shadowPlayer.GetComponent<ShadowPlayer>().FollowPlayer();
        CanMove = true;

    }

    public void SetDestination()
    {
        if (!isMoving)
        {
            //Debug.Log("Setting Destination");
            if (currentSwipeProgress.x > -0.5f && currentSwipeProgress.x < 0.5f)
            {
                // GO Up/Down
                //Debug.Log("Swipe UP/DOWN");
                MoveZ = SetDestinationZ(currentSwipeProgress.y > 0 ? Vector3.forward : Vector3.back, this);
                //TargetMovePosition.y += 0.55f;
                return;
            }

            if (currentSwipeProgress.y > -0.5f && currentSwipeProgress.y < 0.5f)
            {
                //Debug.Log("Swipe Left/Right");
                // GO Left/Right
                MoveX = SetDestinationX(currentSwipeProgress.x > 0 ? Vector3.right : Vector3.left, this);
                //TargetMovePosition.y += 0.55f;
                return;
            }
        }
    }

    public void RotatePlayer()
    {
        if (!isMoving)
        {
            PlayerShadowUI.RotateWithPlayer();
            iTween.RotateAdd(transform.GetChild(1).gameObject, iTween.Hash("amount", new Vector3(0, 90f, 0), "time", 0.15f, "easetype", iTween.EaseType.easeInQuad, "oncomplete", "ResetRotation", "oncompletetarget", gameObject));
        }
    }

    public IEnumerator CantRotateAnimation()
    {
       StartCoroutine(gamemanager.ui.FlashTapToRotateMessage());

        if (!isMoving)
        {
            gamemanager.SoundManager.PlaySound(2);

            if (UIManager.VibrationOn)
            {
                vib.FailToRotate();
            }

            stillRotating = true;
            iTween.RotateAdd(transform.GetChild(1).gameObject, iTween.Hash("amount", new Vector3(0, 5, 0), "time", 0.1f, "easetype", iTween.EaseType.linear));
            yield return new WaitForSeconds(0.05f);
            iTween.RotateAdd(transform.GetChild(1).gameObject, iTween.Hash("amount", new Vector3(0, -7, 0), "time", 0.1f, "easetype", iTween.EaseType.linear));
            yield return new WaitForSeconds(0.05f);
            iTween.RotateAdd(transform.GetChild(1).gameObject, iTween.Hash("amount", new Vector3(0, 4, 0), "time", 0.1f, "easetype", iTween.EaseType.linear));
            yield return new WaitForSeconds(0.05f);
            iTween.RotateAdd(transform.GetChild(1).gameObject, iTween.Hash("amount", new Vector3(0, -3, 0), "time", 0.1f, "easetype", iTween.EaseType.linear));
            yield return new WaitForSeconds(0.1f);
            iTween.RotateAdd(transform.GetChild(1).gameObject, iTween.Hash("amount", new Vector3(0, 1, 0), "time", 0.1f, "easetype", iTween.EaseType.linear, "oncomplete", "ResetUnsuccesfullRotation", "oncompletetarget", gameObject));
            yield return new WaitForSeconds(0.1f);
            transform.GetChild(1).rotation = Quaternion.Euler(0, currentRotation, 0);
        }
    }

    public void ResetUnsuccesfullRotation()
    {
        shadowPlayer.transform.rotation = Quaternion.Euler(0,currentRotation,0);
        stillRotating = false;
    }

    public void ResetRotation()
    {
        currentRotation += 90;
        stillRotating = false;


        for (int i = 0; i < PlayerMagnetsArray.Count; i++)
        {
            GetCurrentCell(new Vector3(Mathf.RoundToInt(PlayerMagnetsArray[i].transform.position.x), Mathf.RoundToInt(PlayerMagnetsArray[i].transform.position.y), Mathf.RoundToInt(PlayerMagnetsArray[i].transform.position.z)), PlayerMagnetsArray[i].gameObject, false);
        }

    }

    public void GravityFall()
    {
        IsFalling = true;
        iTween.MoveTo(gameObject, iTween.Hash("Y", 0.55f, "space", Space.World, "speed", fallSpeed, "easeType", iTween.EaseType.easeInQuart, "oncomplete", "ResetMovement"));
        //iTween.MoveTo(transform.parent.gameObject, iTween.Hash("Y", 0.55f, "space", Space.World, "speed", fallSpeed, "easeType", iTween.EaseType.easeInQuart, "oncomplete", "ResetMovement"));
    }

    public void PlayLightSound()
    {
        gamemanager.SoundManager.PlaySound(8);
    }


}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowFieldMagnet : MonoBehaviour
{
    public GameObject currentParent;

    public Transform OriginalParent;

    public Transform ShadowPlayer;

    public GameObject WallHitDuringRotation;

    public float distanceFromWall;

    private void Start()
    {
        currentParent = transform.parent.gameObject;

        OriginalParent = currentParent.transform;
        ShadowPlayer = GameObject.FindWithTag("Shadow Player").transform;
    }

    public void GetAdopted()
    {
        transform.SetParent(ShadowPlayer);
        transform.localRotation = Quaternion.identity;
        ShadowPlayer.GetComponent<ShadowPlayer>().ShadowMagnetsArray.Add(this);
        currentParent = ShadowPlayer.gameObject;
    }

    public bool OverlapCheck()
    {
        Collider[] cols = Physics.OverlapBox(transform.position, Vector3.one / 3);
        
        foreach (Collider col in cols)
        {
            if (col.CompareTag("Wall") || col.CompareTag("SpecialTempWall"))
            {
                WallHitDuringRotation = col.gameObject;
                return false;
            }
        }
        return true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawCube(transform.position, Vector3.one / 3);
    }
}

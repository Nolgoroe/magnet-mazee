﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class FindBlockToSpawn : MonoBehaviour
{
    public List<WallData> TheLevel = new List<WallData>();

    public GameManager gamemanager;

    public List<GameObject> BlockPrefabs;

    RaycastHit WallHitWall;
    LayerMask layerMask;

    public void Init()
    {
        gamemanager = GameManager.instance;
        TheLevel = new List<WallData>();
        TheLevel.Clear();
    }

    public void ShootRays(WallData Hit, Vector3 BlockPos)
    {

        if (Hit.WallColor == Color.black)
        {
            layerMask = 1 << 9;
            layerMask = ~layerMask;
        }
        else if (Hit.WallColor == Color.white)
        {
            layerMask = 1 << 8;
            layerMask = ~layerMask;
        }
        else
        {
            layerMask = ~0;
        }


        if (Physics.Raycast(Hit.transform.position, Vector3.right, out WallHitWall, 1f, layerMask))
        {
            Debug.DrawRay(Hit.transform.position, Vector3.right);

            if (WallHitWall.transform.CompareTag("TempWall") || WallHitWall.transform.CompareTag("SpecialTempWall"))
            {
                Hit.bools[0]= true;
            }
        }

        if (Physics.Raycast(Hit.transform.position, Vector3.left, out WallHitWall, 1f, layerMask))
        {
            Debug.DrawRay(Hit.transform.position, Vector3.left);

            if (WallHitWall.transform.CompareTag("TempWall") || WallHitWall.transform.CompareTag("SpecialTempWall"))
            {
                Hit.bools[1] = true;
            }
        }

        if (Physics.Raycast(Hit.transform.position, Vector3.forward, out WallHitWall, 1f, layerMask))
        {
            Debug.DrawRay(Hit.transform.position, Vector3.forward);

            if (WallHitWall.transform.CompareTag("TempWall") || WallHitWall.transform.CompareTag("SpecialTempWall"))
            {
                Hit.bools[2] = true;
            }
        }

        if (Physics.Raycast(Hit.transform.position, Vector3.back, out WallHitWall, 1f, layerMask))
        {
            Debug.DrawRay(Hit.transform.position, Vector3.back);

            if (WallHitWall.transform.CompareTag("TempWall") || WallHitWall.transform.CompareTag("SpecialTempWall"))
            {
                Hit.bools[3] = true;
            }
        }

        switch (Hit.bools)
        {
            // one side hit
            case var  b when b.SequenceEqual(new bool[] { true, false, false, false }):
                //Debug.Log("1");
                Hit.blocks = WallData.TypesOfBlocks.Right;
                Hit.Prefab = Resources.Load("Prefabs/Right") as GameObject;
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;
            case var  b when b.SequenceEqual(new bool[] { false, true, false, false }):
                //Debug.Log("2");
                Hit.blocks = WallData.TypesOfBlocks.Left;
                Hit.Prefab = Resources.Load("Prefabs/Left") as GameObject;
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;
            case var  b when b.SequenceEqual(new bool[] { false, false, true, false }):
                //Debug.Log("3");
                Hit.blocks = WallData.TypesOfBlocks.Top;
                Hit.Prefab = Resources.Load("Prefabs/Top") as GameObject;
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;
            case var  b when b.SequenceEqual(new bool[] { false, false, false, true }):
                //Debug.Log("4");
                Hit.blocks = WallData.TypesOfBlocks.Down;
                Hit.Prefab = Resources.Load("Prefabs/Down") as GameObject;
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;

                // two sides hit
            case var b when b.SequenceEqual(new bool[] { true, true, false, false }):
                //Debug.Log("5");
                Hit.blocks = WallData.TypesOfBlocks.LeftRight;
                Hit.Prefab = Resources.Load("Prefabs/LeftRight") as GameObject;
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;
            case var b when b.SequenceEqual(new bool[] { false, false, true, true }):
                //Debug.Log("6");
                Hit.blocks = WallData.TypesOfBlocks.TopDown;
                Hit.Prefab = Resources.Load("Prefabs/TopDown") as GameObject;
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;
            case var b when b.SequenceEqual(new bool[] { true, false, true, false }):
                //Debug.Log("7");
                Hit.blocks = WallData.TypesOfBlocks.RightUp;

                if(BlockPos.x == 0 && BlockPos.z == 0)
                {
                    Hit.Prefab = Resources.Load("Prefabs/RightUp(Rounded)") as GameObject;
                }
                else
                {
                    Hit.Prefab = Resources.Load("Prefabs/RightUp") as GameObject;
                }

                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;
            case var b when b.SequenceEqual(new bool[] { true, false, false, true }):
                //Debug.Log("8");
                Hit.blocks = WallData.TypesOfBlocks.RightDown;

                if(BlockPos.x == 0 && BlockPos.z == gamemanager.GridManager.sizeZ - 1)
                {
                    Hit.Prefab = Resources.Load("Prefabs/RightDown(Rounded)") as GameObject;
                }
                else
                {
                    Hit.Prefab = Resources.Load("Prefabs/RightDown") as GameObject;
                }

                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;
            case var b when b.SequenceEqual(new bool[] { false, true, false, true }):
                //Debug.Log("9");
                Hit.blocks = WallData.TypesOfBlocks.LeftDown;

                if(BlockPos.x == gamemanager.GridManager.sizeX - 1 && BlockPos.z == gamemanager.GridManager.sizeZ - 1)
                {
                    Hit.Prefab = Resources.Load("Prefabs/LeftDown(Rounded)") as GameObject;
                }
                else
                {
                    Hit.Prefab = Resources.Load("Prefabs/LeftDown") as GameObject;
                }

                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;
            case var b when b.SequenceEqual(new bool[] { false, true, true, false }):
                //Debug.Log("10");
                Hit.blocks = WallData.TypesOfBlocks.LeftUp;

                if (BlockPos.x == gamemanager.GridManager.sizeX - 1 && BlockPos.z == 0)
                {
                    Hit.Prefab = Resources.Load("Prefabs/LeftUp(Rounded)") as GameObject;
                }
                else
                {
                    Hit.Prefab = Resources.Load("Prefabs/LeftUp") as GameObject;
                }

                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;

            // three sides hit
            case var b when b.SequenceEqual(new bool[] { true, true, false, true }):
                //Debug.Log("11");
                Hit.blocks = WallData.TypesOfBlocks.LeftDownRight;
                Hit.Prefab = Resources.Load("Prefabs/LeftDownRight") as GameObject;
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;
            case var b when b.SequenceEqual(new bool[] { true, true, true, false }):
                //Debug.Log("12");
                Hit.blocks = WallData.TypesOfBlocks.LeftUpRight;
                Hit.Prefab = Resources.Load("Prefabs/LeftUpRight") as GameObject;
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;
            case var b when b.SequenceEqual(new bool[] { true, false, true, true }):
                //Debug.Log("13");
                Hit.blocks = WallData.TypesOfBlocks.UpRightDown;
                Hit.Prefab = Resources.Load("Prefabs/UpRightDown") as GameObject;
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;
            case var b when b.SequenceEqual(new bool[] { false, true, true, true }):
                //Debug.Log("14");
                Hit.blocks = WallData.TypesOfBlocks.LeftUpDown;
                Hit.Prefab = Resources.Load("Prefabs/LeftUpDown") as GameObject;
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;

            // all sides hit
            case var b when b.SequenceEqual(new bool[] { true, true, true, true }):
                //Debug.Log("15");
                Hit.blocks = WallData.TypesOfBlocks.AllSIdes;
                CheckDiagnal(Hit.transform);
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;

            //no sides hit
            case var b when b.SequenceEqual(new bool[] { false, false, false, false }):
                //Debug.Log("16");
                Hit.blocks = WallData.TypesOfBlocks.NoSides;
                Hit.Prefab = Resources.Load("Prefabs/NoSides") as GameObject;
                CheckIfInArray(Hit);
                Hit.InArray = true;
                break;

            default:
                break;
        }
    }

    public void CheckIfInArray(WallData WallInLevel)
    {
        if (!WallInLevel.InArray)
        {
            WallInLevel.Position = WallInLevel.transform.position;
            //WallInLevel.name = WallInLevel.blocks.ToString();

            if (WallInLevel.CompareTag("SpecialTempWall"))
            {
                SwitchSpecialBlocks(WallInLevel);
            }

            //Debug.Log("Count: " + TheLevel.Count + " " + "Block Inside: " + WallInLevel.gameObject);
            TheLevel.Add(WallInLevel);
          
        }
        else
        {
            Debug.Log(TheLevel.Count);
        }
    }

    public void CheckArrayDone()
    {
        //Debug.Log("Array Done");

        gamemanager.levelgenerator.GenerateActualLevel();
    }

    public void SwitchSpecialBlocks(WallData WallInLevel)
    {
        if (WallInLevel.name == "SpecialLeftRight 90(Clone)")
        {
            if(WallInLevel.blocks == WallData.TypesOfBlocks.TopDown)
            {
                WallInLevel.Prefab = Resources.Load("Prefabs/Special Blocks/SpecialTopDown 90") as GameObject;
            }

            if (WallInLevel.blocks == WallData.TypesOfBlocks.LeftRight)
            {
                WallInLevel.Prefab = Resources.Load("Prefabs/Special Blocks/SpecialLeftRight 90") as GameObject;
            }

            WallInLevel.InArray = true;
            //Debug.Log("Summon " + WallInLevel.Prefab);
        }

        if (WallInLevel.name == "SpecialTopDown 45(Clone)")
        {
            if (WallInLevel.blocks == WallData.TypesOfBlocks.TopDown)
            {
                WallInLevel.Prefab = Resources.Load("Prefabs/Special Blocks/SpecialTopDown 45") as GameObject;
            }

            if (WallInLevel.blocks == WallData.TypesOfBlocks.LeftRight)
            {
                WallInLevel.Prefab = Resources.Load("Prefabs/Special Blocks/SpecialLeftRight 45") as GameObject;
            }

            WallInLevel.InArray = true;
            //Debug.Log("Summon " + WallInLevel.Prefab);
        }

        if (WallInLevel.name == "SpecialTopDown 90(Clone)")
        {
            if (WallInLevel.blocks == WallData.TypesOfBlocks.TopDown)
            {
                WallInLevel.Prefab = Resources.Load("Prefabs/Special Blocks/SpecialTopDown 90") as GameObject;
            }

            if (WallInLevel.blocks == WallData.TypesOfBlocks.LeftRight)
            {
                WallInLevel.Prefab = Resources.Load("Prefabs/Special Blocks/SpecialLeftRight 90") as GameObject;
            }

            WallInLevel.InArray = true;
            //Debug.Log("Summon " + WallInLevel.Prefab);
        }

        if (WallInLevel.name == "SpecialLeftRight 45(Clone)")
        {
            if (WallInLevel.blocks == WallData.TypesOfBlocks.TopDown)
            {
                WallInLevel.Prefab = Resources.Load("Prefabs/Special Blocks/SpecialTopDown 45") as GameObject;
            }

            if (WallInLevel.blocks == WallData.TypesOfBlocks.LeftRight)
            {
                WallInLevel.Prefab = Resources.Load("Prefabs/Special Blocks/SpecialLeftRight 45") as GameObject;
            }

            WallInLevel.InArray = true;
            //Debug.Log("Summon " + WallInLevel.Prefab);
        }
    }

    public void CheckDiagnal(Transform BlockToCheck)
    {
        WallData WallToCheck = BlockToCheck.GetComponent<WallData>();

        RaycastHit HitWallUp;

        for (int i = 0; i < gamemanager.GridManager.CellsInWorld.Count; i++)
        {
            if(gamemanager.GridManager.CellsInWorld[i].CellPos.x == WallToCheck.Position.x - 1 && gamemanager.GridManager.CellsInWorld[i].CellPos.z == WallToCheck.Position.z + 1)
            {
                //Debug.Log("Bool " + gamemanager.GridManager.CellsInWorld[i].isFullWithWall + " GameObject pos " + gamemanager.GridManager.CellsInWorld[i].CellPos);
                if(Physics.Raycast(gamemanager.GridManager.CellsInWorld[i].CellPos, Vector3.up, out HitWallUp, 2f))
                {
                    //Debug.Log("I hit A wall Above me");
                    gamemanager.GridManager.CellsInWorld[i].isFullWithWall = true;
                }

                if ((!gamemanager.GridManager.CellsInWorld[i].isFullWithWall) || (gamemanager.GridManager.CellsInWorld[i].isFullWithWall && HitWallUp.transform.gameObject.layer != BlockToCheck.transform.gameObject.layer))
                {
                    WallToCheck.Prefab = Resources.Load("Prefabs/AllSIdes Upper Left Corner") as GameObject;
                    break;
                }
            }       
            else if(gamemanager.GridManager.CellsInWorld[i].CellPos.x == WallToCheck.Position.x - 1 && gamemanager.GridManager.CellsInWorld[i].CellPos.z == WallToCheck.Position.z - 1)
            {
                //Debug.Log("Bool " + gamemanager.GridManager.CellsInWorld[i].isFullWithWall + " GameObject pos " + gamemanager.GridManager.CellsInWorld[i].CellPos);
                if (Physics.Raycast(gamemanager.GridManager.CellsInWorld[i].CellPos, Vector3.up, out HitWallUp, 2f))
                {
                    //Debug.Log("I hit A wall Above me");
                    gamemanager.GridManager.CellsInWorld[i].isFullWithWall = true;
                }

                if ((!gamemanager.GridManager.CellsInWorld[i].isFullWithWall) || (gamemanager.GridManager.CellsInWorld[i].isFullWithWall && HitWallUp.transform.gameObject.layer != BlockToCheck.transform.gameObject.layer))
                {
                    WallToCheck.Prefab = Resources.Load("Prefabs/AllSIdes Lower Left Corner") as GameObject;
                    break;
                }
            }
            else if(gamemanager.GridManager.CellsInWorld[i].CellPos.x == WallToCheck.Position.x + 1 && gamemanager.GridManager.CellsInWorld[i].CellPos.z == WallToCheck.Position.z - 1)
            {
                //Debug.Log("Bool " + gamemanager.GridManager.CellsInWorld[i].isFullWithWall + " GameObject pos " + gamemanager.GridManager.CellsInWorld[i].CellPos);
                if (Physics.Raycast(gamemanager.GridManager.CellsInWorld[i].CellPos, Vector3.up, out HitWallUp, 2f))
                {
                    //Debug.Log("I hit A wall Above me");
                    gamemanager.GridManager.CellsInWorld[i].isFullWithWall = true;
                }

                if ((!gamemanager.GridManager.CellsInWorld[i].isFullWithWall) || (gamemanager.GridManager.CellsInWorld[i].isFullWithWall && HitWallUp.transform.gameObject.layer != BlockToCheck.transform.gameObject.layer))
                {
                    WallToCheck.Prefab = Resources.Load("Prefabs/AllSIdes Lower Right Corner") as GameObject;
                    break;
                }
            }
            else if(gamemanager.GridManager.CellsInWorld[i].CellPos.x == WallToCheck.Position.x + 1 && gamemanager.GridManager.CellsInWorld[i].CellPos.z == WallToCheck.Position.z + 1)
            {
                //Debug.Log("Bool " + gamemanager.GridManager.CellsInWorld[i].isFullWithWall + " GameObject pos " + gamemanager.GridManager.CellsInWorld[i].CellPos);
                if (Physics.Raycast(gamemanager.GridManager.CellsInWorld[i].CellPos, Vector3.up, out HitWallUp, 2f))
                {
                    //Debug.Log("I hit A wall Above me");
                    gamemanager.GridManager.CellsInWorld[i].isFullWithWall = true;
                }

                if ((!gamemanager.GridManager.CellsInWorld[i].isFullWithWall) || (gamemanager.GridManager.CellsInWorld[i].isFullWithWall && HitWallUp.transform.gameObject.layer != BlockToCheck.transform.gameObject.layer))
                {
                    WallToCheck.Prefab = Resources.Load("Prefabs/AllSIdes Upper Right Corner") as GameObject;
                    break;
                }
            }
            else
            {
                WallToCheck.Prefab = Resources.Load("Prefabs/AllSIdes") as GameObject;
            }
        }

    }
}

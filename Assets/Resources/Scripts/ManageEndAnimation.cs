﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageEndAnimation : MonoBehaviour
{
    public Transform PlayerEndAnimationMagnet;

    public int amounttogetbig;

    public float time;

    public static bool DestroyedAll;

    GameManager gamemanager;

    public void Init()
    {
        DestroyedAll = false;
        PlayerEndAnimationMagnet = GameObject.FindGameObjectWithTag("EndAnimPlayer").transform.parent;

        PlayerEndAnimationMagnet.gameObject.SetActive(true);

        gamemanager = GameManager.instance;
    }

    public void incereasesizes()
    {
        //if (Now)
        //{
        PlayerEndAnimationMagnet.transform.localScale = Vector3.Lerp(PlayerEndAnimationMagnet.transform.localScale, new Vector3(PlayerEndAnimationMagnet.transform.localScale.x + amounttogetbig, PlayerEndAnimationMagnet.transform.localScale.y + amounttogetbig, PlayerEndAnimationMagnet.transform.localScale.z + amounttogetbig), time * Time.deltaTime);

        //foreach (Collider Ball in Player.GetComponent<Scaler>().Balls)
        //{
        //    if (Ball.CompareTag("Ball"))
        //    {
        //        Ball.transform.localScale = Vector3.Lerp(new Vector3(Ball.transform.localScale.x, Ball.transform.localScale.y, Ball.transform.localScale.z), new Vector3(Ball.transform.localScale.x + amounttogetbig, Ball.transform.localScale.y + amounttogetbig, Ball.transform.localScale.z + amounttogetbig), time * Time.deltaTime);
        //    }
        //}

        //}
    }

    public IEnumerator Exploade()
    {
        yield return new WaitForSeconds(0.1f);
        //Now = true;

        if (!PlayerWinAnimation.SpeedUpExplosion)
        {
            Debug.Log("WTF");
            //SoundManager.Audiosource.volume = 0;
            yield return new WaitForSeconds(1f);

            foreach (FieldMagnetsEndAnimation Mag in PlayerEndAnimationMagnet.GetComponent<PlayerWinAnimation>().MagnetizedMags)
            {
                if (Mag.CompareTag("EndAnimFieldMagnet"))
                {
                    Mag.transform.GetComponent<Rigidbody>().isKinematic = false;
                    Mag.transform.parent = null;
                    //Destroy(Mag.gameObject, 3f);
                    Mag.GetComponent<Rigidbody>().AddForce(new Vector3(Mag.transform.position.x + Random.Range(-50, 50), Mag.transform.position.y + Random.Range(-50, 50), Mag.transform.position.z + Random.Range(-50, 50)), ForceMode.Impulse);
                }
            }

            Destroy(PlayerEndAnimationMagnet.transform.root.gameObject);

            Destroy(GameObject.FindGameObjectWithTag("Shadow Player").gameObject);

            transform.GetComponent<GameManager>().ui.EnableWinScreen();

            gamemanager.SoundManager.PlaySound(7);
        }
        else
        {
            gamemanager.SoundManager.StopAllSounds();
            Debug.Log("good WTF");

            Destroy(PlayerEndAnimationMagnet.transform.root.gameObject);

            Destroy(GameObject.FindGameObjectWithTag("Shadow Player").gameObject);
        }


        DestroyedAll = true;
        yield return null;

    }
}

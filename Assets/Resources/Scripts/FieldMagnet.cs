﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldMagnet : Magnets
{
    int MoveX, MoveZ;

    private RaycastHit hit;
    public float rayLengh;
    public float speed;
    //public GameManager gameManager;
    public PlayerController player;
    public GameObject ShadowFieldMagnet;
    public Animation anim;
    public Transform ChildAnimPos;
    public Vibration vib;
    private GameObject vibrationManager;
    private bool DoneReset;
    //public Vector3 TargetMovePosition;
    private static int NumForName = 0;

    private int timeshit = 0;

    public LayerMask LayerToHit;

    public Light EndLevelLight;

    GameManager gamemanager;

    private void Start()
    {
        ShadowFieldMagnet = Instantiate(ShadowFieldMagnet, transform);
        ChildAnimPos = transform.GetChild(1).transform;
    }

    public void InitMagnets()
    {
        EndLevelLight.gameObject.SetActive(false);
        //ShadowFieldMagnet = new ShadowFieldMagnet();
        //ShadowFieldMagnet = Resources.Load("Prefabs/Shadow Magnet") as GameObject;
        timeshit = 0;

        IsPartOfPlayer = false;
        player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
        vibrationManager = GameObject.Find("Vibration Manager");
        vib = vibrationManager.GetComponent<Vibration>();
        GetCurrentCell(transform.position, gameObject, false);
        DoneReset = false;
        transform.name = "Magnet" + NumForName;
        NumForName++;
        IsFalling = false; //TURN THIS OFF IF YOU WANT THIS MAGNET TO FALL AT START OF LEVELS

        //GravityFall(); TURN THIS ON IF YOU WANT THIS MAGNET TO FALL AT START OF LEVELS
        gamemanager = GameManager.instance;
        anim = GetComponent<Animation>();

        anim["Magnets_2_Magnet_VIBRATION"].speed = 1.7f;
        player.anim["Magnets_Player_Player_Magnet_Vibration_ANIM"].speed = 1.7f;
    }

    private void Update()
    {
        if(player != null)
        {
            if (!PlayerController.isMoving && !IsPartOfPlayer)
            {
                LookForPlayer();
            }

            if (PlayerController.isMoving)
            {
                timeshit = 0;
            }
        }
    }

    public void LookForPlayer()
    {
        if (beingMagnetized)
        {
            return;
        }

        if (!IsFalling && !PlayerController.isMoving && !PlayerController.stillRotating)
        {
            if (Physics.Raycast(transform.position, Vector3.right, out hit, rayLengh, LayerToHit))
            {
                if (hit.transform.CompareTag("Player") || hit.transform.CompareTag("Magnet"))
                {
                    timeshit++;
                    //Debug.Log("Times Hit: " + timeshit);

                    if (timeshit > 3)
                    {
                        Debug.DrawRay(transform.position, Vector3.right * rayLengh);
                        beingMagnetized = true;
                        anim.Play("Magnets_2_Magnet_VIBRATION");
                        player.anim.Play("Magnets_Player_Player_Magnet_Vibration_ANIM");

                        if ((hit.transform.CompareTag("Magnet") || hit.transform.CompareTag("Player") && !IsPartOfPlayer))
                        {
                            MoveX = FindSpecificCell(this, hit.transform.GetComponent<Magnets>(), Vector3.right);
                        }
                        else
                        {
                            MoveX = SetDestinationX(Vector3.right, this);
                        }
                        StartCoroutine(MagnetizeX());
                    }


                }
            }

            if (Physics.Raycast(transform.position, Vector3.left, out hit, rayLengh, LayerToHit))
            {
                if (hit.transform.CompareTag("Player") || hit.transform.CompareTag("Magnet"))
                {
                    timeshit++;
                    //Debug.Log("Times Hit: " + timeshit);

                    if (timeshit > 3)
                    {
                        Debug.DrawRay(transform.position, Vector3.left * rayLengh);

                        beingMagnetized = true;
                        anim.Play("Magnets_2_Magnet_VIBRATION");
                        player.anim.Play("Magnets_Player_Player_Magnet_Vibration_ANIM");

                        if ((hit.transform.CompareTag("Magnet") || hit.transform.CompareTag("Player") && !IsPartOfPlayer))
                        {
                            MoveX = FindSpecificCell(this, hit.transform.GetComponent<Magnets>(), Vector3.left);
                        }
                        else
                        {
                            MoveX = SetDestinationX(Vector3.left, this);
                        }


                        StartCoroutine(MagnetizeX());

                    }
                }
            }

            if (Physics.Raycast(transform.position, Vector3.forward, out hit, rayLengh, LayerToHit))
            {
                if (hit.transform.CompareTag("Player") || hit.transform.CompareTag("Magnet"))
                {
                    timeshit++;
                    //Debug.Log("Times Hit: " + timeshit);

                    if (timeshit > 3)
                    {
                        Debug.DrawRay(transform.position, Vector3.forward * rayLengh);

                        beingMagnetized = true;
                        anim.Play("Magnets_2_Magnet_VIBRATION");
                        player.anim.Play("Magnets_Player_Player_Magnet_Vibration_ANIM");

                        if ((hit.transform.CompareTag("Magnet") || hit.transform.CompareTag("Player") && !IsPartOfPlayer))
                        {
                            MoveZ = FindSpecificCell(this, hit.transform.GetComponent<Magnets>(), Vector3.forward);
                        }
                        else
                        {
                            MoveZ = SetDestinationZ(Vector3.forward, this);
                        }
                        StartCoroutine(MagnetizeZ());
                    }
                }
            }

            if (Physics.Raycast(transform.position, Vector3.back, out hit, rayLengh, LayerToHit))
            {
                if (hit.transform.CompareTag("Player") || hit.transform.CompareTag("Magnet"))
                {
                    timeshit++;
                    //Debug.Log("Times Hit: " + timeshit);

                    if (timeshit > 3)
                    {
                        Debug.DrawRay(transform.position, Vector3.back * rayLengh);

                        beingMagnetized = true;
                        anim.Play("Magnets_2_Magnet_VIBRATION");
                        player.anim.Play("Magnets_Player_Player_Magnet_Vibration_ANIM");

                        if ((hit.transform.CompareTag("Magnet") || hit.transform.CompareTag("Player") && !IsPartOfPlayer))
                        {
                            MoveZ = FindSpecificCell(this, hit.transform.GetComponent<Magnets>(), Vector3.back);
                        }
                        else
                        {
                            MoveZ = SetDestinationZ(Vector3.back, this);
                        }

                        StartCoroutine(MagnetizeZ());
                    }

                }
            }


        }

    }

    //public void Magnetize()
    //{
    //    beingMagnetized = true;
    //    player.isMoving = true;
    //    TargetMovePosition.y += 0.55f;
    //    iTween.MoveTo(gameObject, iTween.Hash("position", TargetMovePosition, "speed", speed, "easeType", iTween.EaseType.easeInOutQuart, "oncomplete", "ResetMagnetization"));
    //}

    public IEnumerator MagnetizeX()
    {
        PlayerController.isMoving = true;
        if (MoveX != 0)
        {
            yield return new WaitForSeconds(anim.GetClip("Magnets_2_Magnet_VIBRATION").length - 0.65f);
            timeshit = 0;
            DoneReset = false;
            //Debug.Log("moveing X times");
            //yield return new WaitForSeconds(0.2f);
            //TargetMovePosition.y += 0.55f;
            iTween.MoveBy(gameObject, iTween.Hash("X", MoveX, "space", Space.World, "speed", speed, "easeType", iTween.EaseType.easeInOutQuart, "oncomplete", "ResetMagnetization"));
            yield return null;
        }
        else
        {
            yield return new WaitForSeconds(anim.GetClip("Magnets_2_Magnet_VIBRATION").length - 0.65f);
            anim.Stop("Magnets_2_Magnet_VIBRATION");
            player.anim.Stop("Magnets_Player_Player_Magnet_Vibration_ANIM");
            //Debug.Log( "Befrore " + player.transform.GetChild(0).eulerAngles);
            player.transform.GetChild(1).eulerAngles = new Vector3(0, player.currentRotation, 0);
            //Debug.Log("Aftrer " + player.transform.GetChild(0).eulerAngles);
            player.transform.rotation = Quaternion.identity;
            transform.rotation = Quaternion.identity;
            timeshit = 0;
            DoneReset = false;
            //Debug.Log("moveing Z times");
            //yield return new WaitForSeconds(0.2f);
            //TargetMovePosition.y += 0.55f;
            iTween.MoveBy(gameObject, iTween.Hash("Z", MoveZ, "space", Space.World, "speed", speed, "easeType", iTween.EaseType.easeInOutQuart, "oncomplete", "ResetMagnetization"));
            yield return null;
        }
    }
    public IEnumerator MagnetizeZ()
    {
        PlayerController.isMoving = true;
        if (MoveZ != 0)
        {
            yield return new WaitForSeconds(anim.GetClip("Magnets_2_Magnet_VIBRATION").length - 0.65f);
            timeshit = 0;
            DoneReset = false;
            //Debug.Log("moveing Z times");
            //yield return new WaitForSeconds(0.2f);
            //TargetMovePosition.y += 0.55f;
            iTween.MoveBy(gameObject, iTween.Hash("Z", MoveZ, "space", Space.World, "speed", speed, "easeType", iTween.EaseType.easeInOutQuart, "oncomplete", "ResetMagnetization"));
            yield return null;
        }
        else
        {
            yield return new WaitForSeconds(anim.GetClip("Magnets_2_Magnet_VIBRATION").length - 0.65f);
            anim.Stop("Magnets_2_Magnet_VIBRATION");
            player.anim.Stop("Magnets_Player_Player_Magnet_Vibration_ANIM");
            //Debug.Log("Befrore " + player.transform.GetChild(0).eulerAngles);
            player.transform.GetChild(1).eulerAngles = new Vector3(0, player.currentRotation,0);
            player.transform.rotation = Quaternion.identity;
            //Debug.Log("Aftrer " + player.transform.GetChild(0).eulerAngles);
            transform.rotation = Quaternion.identity;
            timeshit = 0;
            DoneReset = false;
            //Debug.Log("moveing Z times");
            //yield return new WaitForSeconds(0.2f);
            //TargetMovePosition.y += 0.55f;
            iTween.MoveBy(gameObject, iTween.Hash("Z", MoveZ, "space", Space.World, "speed", speed, "easeType", iTween.EaseType.easeInOutQuart, "oncomplete", "ResetMagnetization"));
            yield return null;
        }
    }

    public void ResetMagnetization()
    {
        if (DoneReset)
        {
            return;
        }

        DoneReset = true ;

        gamemanager.SoundManager.PlaySound(1);

        if (UIManager.VibrationOn)
        {
            vib.SuccessfulMagnetization();
        }

        //Debug.Log("Rest Magnetization");
        //transform.tag = "Untagged";
        player.transform.GetChild(1).eulerAngles = new Vector3(0, player.currentRotation, 0);
        player.transform.rotation = Quaternion.identity;
        transform.gameObject.layer = 11;
        CurrentCell.isFullWithMagnet = false;
        GetCurrentCell(transform.position, gameObject, false);
        IsPartOfPlayer = true;
        PlayerController.PlayerMagnetsArray.Add(this);
        player.CheckingList = PlayerController.PlayerMagnetsArray;
        transform.SetParent(player.transform.GetChild(1));
        ShadowFieldMagnet.GetComponent<ShadowFieldMagnet>().GetAdopted();
        beingMagnetized = false;
        MoveX = 0;
        MoveZ = 0;
        gamemanager.AllLevelMagnets.Remove(gameObject);
        gamemanager.CheckAmountOfMagnets();
        PlayerController.isMoving = false;

        if (gamemanager.levelgenerator.CurrentLevel == 3 && PlayerController.PlayerMagnetsArray.Count > 1 && PlayerController.PlayerMagnetsArray.Count < 3)
        {
            gamemanager.ui.MessagesToPlaeyrs.SetActive(true);
            gamemanager.ui.TapToRotate.SetActive(true);
        }

    }

    public void GravityFall()
    {
        IsFalling = true;
        iTween.MoveTo(gameObject, iTween.Hash("Y", 0.55f, "space", Space.World, "speed", fallSpeed, "easeType", iTween.EaseType.easeInQuart, "oncomplete", "CompleteFall"));
    }

    public void CompleteFall()
    {
        IsFalling = false;
    }

}
